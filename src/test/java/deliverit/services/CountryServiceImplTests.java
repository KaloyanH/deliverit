package deliverit.services;

import com.telerikacademy.deliverit.models.Country;
import com.telerikacademy.deliverit.repositories.CountryRepository;
import com.telerikacademy.deliverit.services.CountryServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


import java.util.ArrayList;

import static deliverit.services.Helpers.createMockCountry;

@ExtendWith(MockitoExtension.class)
public class CountryServiceImplTests {

    @Mock
    CountryRepository mockRepository;

    @InjectMocks
    CountryServiceImpl service;

    @Test
    void getAll_should_callRepository() {
        // Arrange
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        service.getAll();

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_returnCountry_when_matchExist() {
        // Arrange

        Country mockCountry = createMockCountry();
        Mockito.when(mockRepository.getById(mockCountry.getId()))
                .thenReturn(mockCountry);
        // Act
        Country result = service.getById(mockCountry.getId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockCountry.getId(), result.getId()),
                () -> Assertions.assertEquals(mockCountry.getName(), result.getName()));
    }

}
