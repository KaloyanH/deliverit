package deliverit.services;

import com.telerikacademy.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.deliverit.exceptions.UnauthorizedOperationException;
import com.telerikacademy.deliverit.models.Role;
import com.telerikacademy.deliverit.models.User;
import com.telerikacademy.deliverit.repositories.RolesRepository;
import com.telerikacademy.deliverit.services.RoleServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static deliverit.services.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class RoleServiceImplTests {

    @Mock
    RolesRepository mockRepository;

    @InjectMocks
    RoleServiceImpl service;

    @Test
    void getAll_should_callRepository() {
        // Arrange
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        service.getAll();

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_returnRole_when_matchExist() {
        // Arrange
        Role mockRole = createMockRole("User");
        Mockito.when(mockRepository.getById(mockRole.getId()))
                .thenReturn(mockRole);
        // Act
        Role result = service.getById(mockRole.getId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockRole.getId(), result.getId()),
                () -> Assertions.assertEquals(mockRole.getRole(), result.getRole()));
    }

    @Test
    public void create_should_throw_when_RoleWithSameNameExists() {
        // Arrange
        Role mockRole = createMockRole("User");
        User mockEmployee = createMockEmployee();
        Mockito.when(mockRepository.getByRole(mockRole.getRole()))
                .thenReturn(mockRole);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.create(mockRole, mockEmployee));
    }


    @Test
    public void create_should_callRepository_when_roleWithSameNameDoesNotExist() {
        // Arrange
        Role mockRole = createMockRole("User");
        User mockEmployee = createMockEmployee();

        Mockito.when(mockRepository.getByRole(mockRole.getRole()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        service.create(mockRole, mockEmployee);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockRole);
    }

    @Test
    public void update_should_throwException_when_roleNameIsTaken() {
        // Arrange
        var mockRole = createMockRole("User");
        var anotherMockRole = createMockRole("User");
        anotherMockRole.setId(2);
        User mockEmployee = createMockEmployee();

        Mockito.when(mockRepository.getByRole(Mockito.anyString()))
                .thenReturn(anotherMockRole);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.update(mockRole, mockEmployee));

    }

    @Test
    public void update_should_callRepository_when_tryingToUpdateExistingRole() {
        // Arrange
        var mockRole = createMockRole("User");

        User mockEmployee = createMockEmployee();
        Mockito.when(mockRepository.getByRole(Mockito.anyString()))
                .thenReturn(mockRole);


        // Act
        service.update(mockRole, mockEmployee);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockRole);
    }

    @Test
    void delete_should_throwException_when_initiatorIsNotEmployee() {
        // Arrange
        var mockRole = createMockRole("User");
        User mockInitiator = createMockCustomer();
        mockInitiator.setFirstName("MockInitiator");

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(mockRole.getId(), mockInitiator));
    }
}
