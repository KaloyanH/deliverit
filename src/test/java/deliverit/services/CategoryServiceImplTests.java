package deliverit.services;

import com.telerikacademy.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.deliverit.exceptions.UnauthorizedOperationException;
import com.telerikacademy.deliverit.models.Category;
import com.telerikacademy.deliverit.models.User;
import com.telerikacademy.deliverit.repositories.CategoryRepository;
import com.telerikacademy.deliverit.services.CategoryServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static deliverit.services.Helpers.*;


@ExtendWith(MockitoExtension.class)
public class CategoryServiceImplTests {

    @Mock
    CategoryRepository categoryRepository;

    @InjectMocks
    CategoryServiceImpl service;

    @Test
    void getAll_should_callRepository() {
        // Arrange
        Mockito.when(categoryRepository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        service.getAll();

        // Assert
        Mockito.verify(categoryRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_returnCategory_when_matchExist() {
        // Arrange
        Category mockCategory = createMockCategory();
        Mockito.when(categoryRepository.getById(mockCategory.getId()))
                .thenReturn(mockCategory);
        // Act
        Category result = service.getById(mockCategory.getId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockCategory.getId(), result.getId()),
                () -> Assertions.assertEquals(mockCategory.getName(), result.getName()));
    }

    @Test
    public void create_should_throw_when_CategoryWithSameNameExists() {
        // Arrange
        Category mockCategory = createMockCategory();

        Mockito.when(categoryRepository.getByName(mockCategory.getName()))
                .thenReturn(mockCategory);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.create(mockCategory));
    }


    @Test
    public void create_should_callRepository_when_categoryWithSameNameDoesNotExist() {
        // Arrange
        Category mockCategory = createMockCategory();

        Mockito.when(categoryRepository.getByName(mockCategory.getName()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        service.create(mockCategory);

        // Assert
        Mockito.verify(categoryRepository, Mockito.times(1))
                .create(mockCategory);
    }

    @Test
    public void update_should_throwException_when_categoryNameIsTaken() {
        // Arrange
        var mockCategory = createMockCategory();
        var anotherMockCategory = createMockCategory();
        anotherMockCategory.setId(2);

        Mockito.when(categoryRepository.getByName(Mockito.anyString()))
                .thenReturn(anotherMockCategory);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.update(mockCategory));

    }

    @Test
    public void update_should_callRepository_when_tryingToUpdateExistingCategory() {
        // Arrange
        Category mockCategory = createMockCategory();


        Mockito.when(categoryRepository.getByName(Mockito.anyString()))
                .thenReturn(mockCategory);


        // Act
        service.update(mockCategory);

        // Assert
        Mockito.verify(categoryRepository, Mockito.times(1))
                .update(mockCategory);
    }

    @Test
    void delete_should_throwException_when_initiatorIsNotEmployee() {
        // Arrange
        Category mockCategory = createMockCategory();
        User mockInitiator = createMockCustomer();
        mockInitiator.setFirstName("MockInitiator");

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(mockCategory.getId(), mockInitiator));

    }
}
