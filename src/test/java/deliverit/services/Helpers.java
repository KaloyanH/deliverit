package deliverit.services;


import com.telerikacademy.deliverit.models.*;


import java.time.LocalDate;
import java.util.Set;

public class Helpers {

    public static User createMockCustomer() {
        return createMockCustomer("customer");
    }

    public static User createMockEmployee() {
        return createMockCustomer("employee");
    }

    public static Category createMockCategory() {
    var mockCategory = new Category();
    mockCategory.setId(1);
    mockCategory.setName("Food");
    return mockCategory;
}

    public static User createMockCustomer(String role) {
        var mockUser = new User();
        mockUser.setId(1);
        mockUser.setEmail("mock@user.com");
        mockUser.setFirstName("MockFirstName");
        mockUser.setLastName("MockLastName");
        mockUser.setAddress(createMockAddress());
        mockUser.setRoles(Set.of(createMockRole(role)));
        mockUser.setRole(createMockRole(role));
        return mockUser;
    }



    public static CollectionParcel createMockCollectionParcel() {
        var mockCollectionParcel = new CollectionParcel();
        mockCollectionParcel.setId(1);
        mockCollectionParcel.setParcel(createMockParcel());
        mockCollectionParcel.setShipment(createMockShipment());

        return mockCollectionParcel;
    }

    public static Parcel createMockParcel() {
        var mockParcel = new Parcel();
        mockParcel.setId(1);
        mockParcel.setName("Apples");
        mockParcel.setUser(createMockCustomer());
        mockParcel.setCategory(createMockCategory());
        mockParcel.setWeight(1);
        mockParcel.setWarehouse(createMockWarehouse());
        return mockParcel;
    }

    public static Role createMockRole(String role) {
        var mockRole = new Role();
        mockRole.setId(1);
        mockRole.setRole(role);
        return mockRole;
    }

    public static Address createMockAddress() {
        var mockAddress = new Address();
        mockAddress.setId(1);
        mockAddress.setName("TestAddress");
        mockAddress.setCity(createMockCity("Sofia"));
        return mockAddress;
    }

    public static Shipment createMockShipment() {
        var mockShipment = new Shipment();

        mockShipment.setId(1);
        mockShipment.setOriginWarehouse(createMockWarehouse());
        mockShipment.setDestinationWarehouse(createMockWarehouse());
        mockShipment.setDeparture(LocalDate.now());
        mockShipment.setArrival(mockShipment.getDeparture().plusDays(5));
        return mockShipment;
    }

    public static Warehouse createMockWarehouse() {
        var mockWarehouse = new Warehouse();

        mockWarehouse.setId(1);
        mockWarehouse.setAddress(createMockAddress());
        mockWarehouse.setName("Uni Masters Logistics");
        return mockWarehouse;
    }

    public static City createMockCity(String city) {

        var mockCity = new City();
        mockCity.setId(1);
        mockCity.setName(city);
        mockCity.setCountry(createMockCountry());
        return mockCity;
    }

    public static Country createMockCountry() {
        var mockCountry = new Country();
        mockCountry.setId(1);
        mockCountry.setName("Jordania");
        return mockCountry;
    }

}
