package deliverit.services;

import com.telerikacademy.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.deliverit.exceptions.UnauthorizedOperationException;
import com.telerikacademy.deliverit.models.Address;
import com.telerikacademy.deliverit.models.User;
import com.telerikacademy.deliverit.repositories.AddressRepository;
import com.telerikacademy.deliverit.services.AddressServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static deliverit.services.Helpers.createMockAddress;
import static deliverit.services.Helpers.createMockCustomer;


@ExtendWith(MockitoExtension.class)
public class AddressServiceImplTests {

    @Mock
    AddressRepository mockRepository;

    @InjectMocks
    AddressServiceImpl service;

    @Test
    void getAll_should_callRepository() {
        // Arrange
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        service.getAll();

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_returnAddress_when_matchExist() {
        // Arrange
        Address mockAddress = createMockAddress();
        Mockito.when(mockRepository.getById(mockAddress.getId()))
                .thenReturn(mockAddress);
        // Act
        Address result = service.getById(mockAddress.getId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockAddress.getId(), result.getId()),
                () -> Assertions.assertEquals(mockAddress.getName(), result.getName()),
                () -> Assertions.assertEquals(mockAddress.getCity(), result.getCity())
        );
    }

    @Test
    public void create_should_throw_when_AddressWithSameNameExists() {
        // Arrange
        Address mockAddress = createMockAddress();

        Mockito.when(mockRepository.getByName(mockAddress.getName()))
                .thenReturn(mockAddress);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.create(mockAddress));
    }


    @Test
    public void create_should_callRepository_when_addressWithSameNameDoesNotExist() {
        // Arrange
        Address mockAddress = createMockAddress();

        Mockito.when(mockRepository.getByName(mockAddress.getName()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        service.create(mockAddress);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockAddress);
    }

    @Test
    public void update_should_throwException_when_addressNameIsTaken() {
        // Arrange
        var mockAddress = createMockAddress();
        var anotherMockAddress = createMockAddress();
        anotherMockAddress.setId(2);


        Mockito.when(mockRepository.getByName(Mockito.anyString()))
                .thenReturn(anotherMockAddress);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.update(mockAddress));

    }

    @Test
    public void update_should_callRepository_when_tryingToUpdateExistingAddress() {
        // Arrange
        Address mockAddress = createMockAddress();


        Mockito.when(mockRepository.getByName(Mockito.anyString()))
                .thenReturn(mockAddress);


        // Act
        service.update(mockAddress);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockAddress);
    }

    @Test
    void delete_should_throwException_when_initiatorIsNotEmployee() {
        // Arrange
        Address mockAddress = createMockAddress();
        User mockInitiator = createMockCustomer();
        mockInitiator.setFirstName("MockInitiator");


        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(mockAddress.getId(), mockInitiator));
    }


}
