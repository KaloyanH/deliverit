package deliverit.services;


import com.telerikacademy.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.deliverit.models.Parcel;
import com.telerikacademy.deliverit.models.User;
import com.telerikacademy.deliverit.repositories.ParcelRepository;
import com.telerikacademy.deliverit.services.ParcelServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


import java.util.ArrayList;
import java.util.Optional;

import static deliverit.services.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class ParcelServiceImplTests {

    @Mock
    ParcelRepository mockRepository;

    @InjectMocks
    ParcelServiceImpl service;

    @Test
    void getAll_should_callRepository() {
        // Arrange
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());
        // Act
        service.getAll();

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_returnParcel_when_matchExist() {
        // Arrange
        Parcel mockParcel = createMockParcel();

        Mockito.when(mockRepository.getById(mockParcel.getId()))
                .thenReturn(mockParcel);
        // Act
        Parcel result = service.getById(mockParcel.getId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockParcel.getId(), result.getId()),
                () -> Assertions.assertEquals(mockParcel.getName(), result.getName()),
                () -> Assertions.assertEquals(mockParcel.getUser(), result.getUser()),
                () -> Assertions.assertEquals(mockParcel.getWarehouse(), result.getWarehouse()),
                () -> Assertions.assertEquals(mockParcel.getWeight(), result.getWeight()),
                () -> Assertions.assertEquals(mockParcel.getCategory(), result.getCategory())
        );
    }

    @Test
    public void getByName_should_returnParcel_when_matchExist() {
        // Arrange
        Parcel mockParcel = createMockParcel();
        Mockito.when(mockRepository.getByName(mockParcel.getName()))
                .thenReturn(mockParcel);

        // Act
        Parcel result = service.getByName(mockParcel.getName());
        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockParcel.getName(), result.getName())
        );
    }

    @Test
    void filterByWeight_should_callRepository() {
        // Arrange
        Mockito.when(mockRepository.filterByWeight(Optional.empty()))
                .thenReturn(new ArrayList<>());

        // Act
        service.filterByWeight(Optional.empty());

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .filterByWeight(Optional.empty());
    }

    @Test
    void filterByEmail_should_callRepository() {
        // Arrange
        Mockito.when(mockRepository.filterByEmail(Optional.empty()))
                .thenReturn(new ArrayList<>());

        // Act
        service.filterByEmail(Optional.empty());

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .filterByEmail(Optional.empty());
    }

    @Test
    void filterByWarehouse_should_callRepository() {
        // Arrange
        Mockito.when(mockRepository.filterByWarehouse(Optional.empty()))
                .thenReturn(new ArrayList<>());

        // Act
        service.filterByWarehouse(Optional.empty());

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .filterByWarehouse(Optional.empty());
    }

    @Test
    void filterByCategory_should_callRepository() {
        // Arrange
        Mockito.when(mockRepository.filterByCategory(Optional.empty()))
                .thenReturn(new ArrayList<>());

        // Act
        service.filterByCategory(Optional.empty());

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .filterByCategory(Optional.empty());
    }

    @Test
    void filter_should_callRepository() {
        // Arrange
        Mockito.when(mockRepository.filter(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty()) )
                .thenReturn(new ArrayList<>());

        // Act
        service.filter(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty());

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .filter(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty());
    }

    @Test
    public void create_should_throw_when_parcelWithSameIdExists() {
        // Arrange
        User mockUser = createMockEmployee();
        Parcel mockParcel = createMockParcel();



        Mockito.when(mockRepository.getById(mockParcel.getId()))
                .thenReturn(mockParcel);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.create(mockParcel, mockUser));
    }

    @Test
    void create_should_callRepository_when_parcelWithSameIdDoesNotExist() {
        // Arrange
        User mockUser = createMockEmployee();
        Parcel mockParcel = createMockParcel();

        Mockito.when(mockRepository.getById(mockParcel.getId()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        service.create(mockParcel, mockUser);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockParcel);
    }

    @Test
    void update_should_callRepository_when_parcelIsCreatorAndIsNotAdmin() {
        // Arrange
        Parcel mockParcel = createMockParcel();
        User mockUser = createMockEmployee();


        Mockito.when(mockRepository.getById(mockParcel.getId()))
                .thenReturn(mockParcel);

        // Act
        service.update(mockParcel, mockUser);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockParcel);
    }
}
