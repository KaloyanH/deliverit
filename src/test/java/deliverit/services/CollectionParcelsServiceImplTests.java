package deliverit.services;


import com.telerikacademy.deliverit.models.CollectionParcel;
import com.telerikacademy.deliverit.repositories.CollectionParcelRepository;
import com.telerikacademy.deliverit.services.CollectionParcelsServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static deliverit.services.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class CollectionParcelsServiceImplTests {

    @Mock
    CollectionParcelRepository mockRepository;

    @InjectMocks
    CollectionParcelsServiceImpl service;

    @Test
    void getAll_should_callRepository() {
        // Arrange
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        service.getAll();

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_returnCollectionParcel_when_matchExist() {
        // Arrange
        CollectionParcel collectionParcel = createMockCollectionParcel();
        Mockito.when(mockRepository.getById(collectionParcel.getId()))
                .thenReturn(collectionParcel);
        // Act
        CollectionParcel result = service.getById(collectionParcel.getId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(collectionParcel.getId(), result.getId()),
                () -> Assertions.assertEquals(collectionParcel.getParcel(), result.getParcel()),
                () -> Assertions.assertEquals(collectionParcel.getShipment(), result.getShipment())
        );
    }


}
