package deliverit.services;

import com.telerikacademy.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.deliverit.exceptions.UnauthorizedOperationException;
import com.telerikacademy.deliverit.models.User;
import com.telerikacademy.deliverit.repositories.UserRepository;
import com.telerikacademy.deliverit.services.UserServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

import static deliverit.services.Helpers.*;


@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {

    @Mock
    UserRepository mockRepository;

    @InjectMocks
    UserServiceImpl service;

    @Test
    void getAll_should_callRepository() {
        // Arrange
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());
        Optional<String> search = Optional.of("Users");
        // Act
        service.getAll(search);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getByFirstName_should_returnUser_when_matchExist() {
        // Arrange
        User mockUser = createMockCustomer();
        Mockito.when(mockRepository.getByFirstName(mockUser.getFirstName()))
                .thenReturn(mockUser);

        // Act
        User result = service.getByFirstName(mockUser.getFirstName());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getFirstName(), result.getFirstName())
        );
    }

    @Test
    public void getByEmail_should_returnUser_when_matchExist() {
        // Arrange
        User mockUser = createMockCustomer();
        Mockito.when(mockRepository.getByEmail(mockUser.getEmail()))
                .thenReturn(mockUser);

        // Act
        User result = service.getByEmail(mockUser.getEmail());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getEmail(), result.getEmail())
        );
    }



    @Test
    public void getById_should_returnUser_when_matchExist() {
        // Arrange
        User mockUser = createMockCustomer();
        Mockito.when(mockRepository.getById(mockUser.getById()))
                .thenReturn(mockUser);
        // Act
        User result = service.getById(mockUser.getById());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getById(), result.getById()),
                () -> Assertions.assertEquals(mockUser.getAddress(), result.getAddress()),
                () -> Assertions.assertEquals(mockUser.getFirstName(), result.getFirstName()),
                () -> Assertions.assertEquals(mockUser.getLastName(), result.getLastName()),
                () -> Assertions.assertEquals(mockUser.getEmail(), result.getEmail()),
                        () -> Assertions.assertEquals(mockUser.getRoles(), result.getRoles())
        );
    }

    @Test
    public void create_should_throw_when_UserWithSameNameExists() {
        // Arrange
        User mockUser = createMockCustomer();

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.create(mockUser));
    }


    @Test
    public void create_should_callRepository_when_userWithSameNameDoesNotExist() {
        // Arrange
        User mockUser = createMockCustomer();

        Mockito.when(mockRepository.getByEmail(mockUser.getEmail()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        service.create(mockUser);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockUser);
    }


    @Test
    void delete_should_throwException_when_initiatorIsNotEmployee() {
        // Arrange
        User mockUser = createMockCustomer();
        User mockInitiator = createMockCustomer();
        mockInitiator.setFirstName("MockInitiator");
        
        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(mockUser.getById(), mockInitiator));
    }

    @Test
    void list_should_callRepository() {
        User mockUser = createMockCustomer();
        // Act
        service.listUserParcels(mockUser.getById());

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .listUserParcels(mockUser.getById());
    }

    @Test
    void listUserCompletedParcels_should_callRepository() {
        User mockUser = createMockCustomer();
        // Act
        service.listUserCompletedParcels(mockUser.getById());

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .listUserCompletedParcels(mockUser.getById());
    }

    @Test
    void listUserOnTheWayParcels_should_callRepository() {
        User mockUser = createMockCustomer();
        // Act
        service.listUserOnTheWayParcels(mockUser.getById());

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .listUserOnTheWayParcels(mockUser.getById());
    }

    @Test
    void search_should_callRepository() {
        Optional<String> search = Optional.of("Users");
        User mockEmployee = createMockEmployee();
        // Act
        // Arrange
        Mockito.when(mockRepository.search(search, search, search, search))
                .thenReturn(new ArrayList<>());

        // Act
        service.search(search, search, search, search, mockEmployee);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .search(search, search, search, search);
    }

    @Test
    public void update_should_callRepository_when_tryingToUpdateExistingUser() {
        // Arrange
        User mockUser = createMockCustomer();
        User mockEmployee = createMockEmployee();

        Mockito.when(mockRepository.getByFirstName(Mockito.anyString()))
                .thenReturn(mockUser);

        // Act
        service.update(mockUser, mockEmployee);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockUser);

    }


    @Test
    public void update_should_throwException_when_userIsNotEmployee() {
        // Arrange
        User mockUser = createMockCustomer();
        User mockInitiator = createMockCustomer();
        mockInitiator.setFirstName("MockInitiator");

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(mockUser.getById(), mockInitiator));
        // Act, Assert
    }

}
