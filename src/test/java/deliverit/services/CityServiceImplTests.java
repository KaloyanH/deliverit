package deliverit.services;


import com.telerikacademy.deliverit.models.City;
import com.telerikacademy.deliverit.models.Country;
import com.telerikacademy.deliverit.repositories.CityRepository;
import com.telerikacademy.deliverit.services.CityServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static deliverit.services.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class CityServiceImplTests {
    @Mock
    CityRepository mockRepository;

    @InjectMocks
    CityServiceImpl service;

    @Test
    void getAll_should_callRepository() {
        // Arrange
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        service.getAll();

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_returnCity_when_matchExist() {
        // Arrange
        City mockCity = createMockCity("Sofia");
        Country mockCountry = createMockCountry();
        Mockito.when(mockRepository.getById(mockCity.getId()))
                .thenReturn(mockCity);
        // Act
        City result = service.getById(mockCity.getId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockCity.getId(), result.getId()),
                () -> Assertions.assertEquals(mockCity.getName(), result.getName()),
                () -> Assertions.assertEquals(mockCity.getCountry(), result.getCountry())
        );
    }

}
