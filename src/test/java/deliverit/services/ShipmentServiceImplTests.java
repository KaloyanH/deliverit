package deliverit.services;

import com.telerikacademy.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.deliverit.exceptions.UnauthorizedOperationException;
import com.telerikacademy.deliverit.models.Parcel;
import com.telerikacademy.deliverit.models.Shipment;
import com.telerikacademy.deliverit.models.User;
import com.telerikacademy.deliverit.repositories.ShipmentRepository;
import com.telerikacademy.deliverit.services.ShipmentServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

import static deliverit.services.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class ShipmentServiceImplTests {

    @Mock
    ShipmentRepository mockRepository;

    @InjectMocks
    ShipmentServiceImpl service;

    @Test
    void getAll_should_callRepository() {
        // Arrange
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        service.getAll();

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_returnShipment_when_matchExist() {
        // Arrange
        Shipment mockShipment = createMockShipment();
        Mockito.when(mockRepository.getById(mockShipment.getId()))
                .thenReturn(mockShipment);
        // Act
        Shipment result = service.getById(mockShipment.getId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockShipment.getId(), result.getId()),
                () -> Assertions.assertEquals(mockShipment.getOriginWarehouse(), result.getOriginWarehouse()),
                () -> Assertions.assertEquals(mockShipment.getDestinationWarehouse(), result.getDestinationWarehouse()),
                () -> Assertions.assertEquals(mockShipment.getDeparture(), result.getDeparture()),
                () -> Assertions.assertEquals(mockShipment.getArrival(), result.getArrival())
        );
    }

    @Test
    public void create_should_throw_when_ShipmentWithSameNameExists() {
        // Arrange
        Shipment mockShipment = createMockShipment();
        User employee = createMockEmployee();

        Mockito.when(mockRepository.getById(mockShipment.getId()))
                .thenReturn(mockShipment);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.create(mockShipment, employee));
    }


    @Test
    public void create_should_callRepository_when_shipmentWithSameNameDoesNotExist() {
        // Arrange
        Shipment mockShipment = createMockShipment();
        User employee = createMockEmployee();
        Mockito.when(mockRepository.getById(mockShipment.getId()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        service.create(mockShipment, employee);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockShipment);
    }


    @Test
    void delete_should_throwException_when_initiatorIsNotEmployee() {
        // Arrange
        Shipment mockShipment = createMockShipment();
        User mockInitiator = createMockCustomer();
        mockInitiator.setFirstName("MockInitiator");

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(mockShipment.getId(), mockInitiator));
    }

    @Test
    void filterByCustomer_should_callRepository() {
        // Arrange
        Mockito.when(mockRepository.filterByCustomer(Optional.empty()))
                .thenReturn(new ArrayList<>());

        // Act
        service.filterByCustomer(Optional.empty());

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .filterByCustomer(Optional.empty());
    }

    @Test
    void filterByWarehouse_should_callRepository() {
        // Arrange
        Mockito.when(mockRepository.filterByWarehouse(Optional.empty(), Optional.empty()))
                .thenReturn(new ArrayList<>());

        // Act
        service.filterByWarehouse(Optional.empty(), Optional.empty());

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .filterByWarehouse(Optional.empty(), Optional.empty());
    }

    @Test
    void update_should_callRepository_when_shipmentIsCreatorAndIsNotAdmin() {
        // Arrange
        Shipment mockShipment = createMockShipment();
        User employee = createMockEmployee();

        // Act
        service.update(mockShipment, employee);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockShipment);
    }

    @Test
    void listShipmentPreparing_should_callRepository() {
        User mockEmployee = createMockEmployee();
        // Arrange
        Mockito.when(mockRepository.listShipmentPreparing())
                .thenReturn(new ArrayList<>());

        // Act
        service.listShipmentPreparing(mockEmployee);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .listShipmentPreparing();
    }

    @Test
    void listShipmentCompleted_should_callRepository() {
        User mockEmployee = createMockEmployee();
        // Arrange
        Mockito.when(mockRepository.listShipmentCompleted())
                .thenReturn(new ArrayList<>());

        // Act
        service.listShipmentCompleted(mockEmployee);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .listShipmentCompleted();
    }

    @Test
    void listShipmentOnTheWay_should_callRepository() {
        User mockEmployee = createMockEmployee();
        // Arrange
        Mockito.when(mockRepository.listShipmentOnTheWay())
                .thenReturn(new ArrayList<>());

        // Act
        service.listShipmentOnTheWay(mockEmployee);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .listShipmentOnTheWay();
    }
}
