package deliverit.services;

import com.telerikacademy.deliverit.models.Warehouse;
import com.telerikacademy.deliverit.repositories.WarehouseRepository;
import com.telerikacademy.deliverit.services.WarehouseServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static deliverit.services.Helpers.*;

@ExtendWith({MockitoExtension.class})
public class WarehouseServiceImplTests {

    @Mock
    WarehouseRepository mockRepository;

    @InjectMocks
    WarehouseServiceImpl service;

    @Test
    void getAll_should_callRepository() {
        // Arrange
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        service.getAll();

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_returnWarehouse_when_matchExist() {
        // Arrange
        Warehouse mockCity = createMockWarehouse();
        Mockito.when(mockRepository.getById(mockCity.getId()))
                .thenReturn(mockCity);
        // Act
        Warehouse result = service.getById(mockCity.getId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockCity.getId(), result.getId()),
                () -> Assertions.assertEquals(mockCity.getName(), result.getName()),
                () -> Assertions.assertEquals(mockCity.getAddress(), result.getAddress())
        );
    }

}
