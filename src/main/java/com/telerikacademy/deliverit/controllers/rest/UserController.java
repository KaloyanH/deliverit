package com.telerikacademy.deliverit.controllers.rest;

import com.telerikacademy.deliverit.controllers.AuthenticationHelper;
import com.telerikacademy.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.deliverit.models.Parcel;
import com.telerikacademy.deliverit.services.ModelMapperUser;
import com.telerikacademy.deliverit.services.UserService;
import com.telerikacademy.deliverit.models.User;
import com.telerikacademy.deliverit.models.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;


import javax.validation.Valid;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService service;
    private final AuthenticationHelper authenticationHelper;
    private final ModelMapperUser modelMapperUser;

    @Autowired
    public UserController(UserService service, AuthenticationHelper authenticationHelper, ModelMapperUser modelMapperUser) {
        this.service = service;
        this.authenticationHelper = authenticationHelper;
        this.modelMapperUser = modelMapperUser;
    }

    @GetMapping
    public List<User> getAll(@RequestHeader HttpHeaders headers,@RequestParam(required = false) Optional<String> search,
                             @RequestParam(required = false) Optional<String> firstName,
                             @RequestParam(required = false) Optional<String> lastName,
                             @RequestParam(required = false) Optional<String> email) {
        User employee = authenticationHelper.tryGetUser(headers);
        return service.search(search, firstName, lastName, email, employee);
    }

    @GetMapping("/customers")
    public List<User> getAllCustomers() {
        return service.getAllCustomers();
    }

    @GetMapping("/{id}")
    public User getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public User create(@Valid @RequestBody UserDto userDto) {
        try {
            User user = modelMapperUser.fromDto(userDto);
            service.create(user);
            return user;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public User update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody UserDto userDto) {
        try {
            User employee = authenticationHelper.tryGetUser(headers);
            User user = modelMapperUser.fromDto(userDto, id);
            service.update(user, employee);
            return user;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public User delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            service.delete(id, user);
            return user;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/list/userparcel")
    public List<Parcel> list(@RequestParam int userId) {

        if (service.listUserParcels(userId).isEmpty()){
            throw new ResponseStatusException((HttpStatus.NOT_FOUND));
        }
        return service.listUserParcels(userId);
    }



}
