package com.telerikacademy.deliverit.controllers.rest;

import com.telerikacademy.deliverit.controllers.AuthenticationHelper;
import com.telerikacademy.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.deliverit.models.CollectionParcel;
import com.telerikacademy.deliverit.models.User;
import com.telerikacademy.deliverit.services.CollectionParcelsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/collections_parcels")
public class CollectionParcelsController {


    private final CollectionParcelsService collectionParcelsService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CollectionParcelsController(CollectionParcelsService collectionParcelsService, AuthenticationHelper authenticationHelper) {
        this.collectionParcelsService = collectionParcelsService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<CollectionParcel> getAll(@RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);

            return collectionParcelsService.getAll();
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }

    @GetMapping("/{id}")
    public CollectionParcel getById(@PathVariable int id){
        try {
            return collectionParcelsService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
