package com.telerikacademy.deliverit.controllers.rest;

import com.telerikacademy.deliverit.controllers.AuthenticationHelper;
import com.telerikacademy.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.deliverit.models.User;
import com.telerikacademy.deliverit.services.ModelMapperCategory;
import com.telerikacademy.deliverit.models.Category;
import com.telerikacademy.deliverit.models.dto.CategoryDto;
import com.telerikacademy.deliverit.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/categories")
public class CategoryController {

    private final CategoryService service;
    private final AuthenticationHelper authenticationHelper;
    private final ModelMapperCategory modelMapperCategory;

    @Autowired
    public CategoryController(CategoryService service, AuthenticationHelper authenticationHelper, ModelMapperCategory modelMapperCategory) {
        this.service = service;
        this.authenticationHelper = authenticationHelper;
        this.modelMapperCategory = modelMapperCategory;
    }

    @GetMapping
    public List<Category> getAll() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    public Category getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Category create(@Valid @RequestBody CategoryDto categoryDto) {
        try {
            Category category = modelMapperCategory.fromDto(categoryDto);
            service.create(category);
            return category;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Category update(@PathVariable int id, @Valid @RequestBody CategoryDto categoryDto) {
        try {
            Category category = modelMapperCategory.fromDto(categoryDto, id);
            service.update(category);
            return category;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public Category delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Category category = service.getById(id);
            service.delete(id, user);
            return category;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

}
