package com.telerikacademy.deliverit.controllers.rest;

import com.telerikacademy.deliverit.controllers.AuthenticationHelper;
import com.telerikacademy.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.deliverit.exceptions.UnauthorizedOperationException;
import com.telerikacademy.deliverit.models.User;
import com.telerikacademy.deliverit.services.ModelMapperShipment;
import com.telerikacademy.deliverit.services.ShipmentService;
import com.telerikacademy.deliverit.models.CollectionParcel;
import com.telerikacademy.deliverit.models.Shipment;
import com.telerikacademy.deliverit.models.dto.ShipmentDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/shipments")
public class ShipmentController {


    private final ShipmentService service;
    private final AuthenticationHelper authenticationHelper;
    private final ModelMapperShipment modelMapperShipment;

    @Autowired
    public ShipmentController(ShipmentService service, AuthenticationHelper authenticationHelper, ModelMapperShipment modelMapperShipment) {
        this.service = service;
        this.authenticationHelper = authenticationHelper;
        this.modelMapperShipment = modelMapperShipment;

    }

    @GetMapping
    public List<Shipment> getAll() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    public Shipment getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Shipment create(@RequestHeader HttpHeaders headers, @Valid @RequestBody ShipmentDto shipmentDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Shipment shipment = modelMapperShipment.fromDto(shipmentDto);
            service.create(shipment, user);
            return shipment;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
    }

    @PutMapping("/{id}")
    public Shipment update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody ShipmentDto shipmentDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Shipment shipment = modelMapperShipment.fromDto(shipmentDto, id);
            service.update(shipment, user);
            return shipment;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Shipment shipment = service.getById(id);
            service.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/filter/warehouse")
    public List<Shipment> filterByWarehouse(@RequestParam(required = false) Optional<Integer> destinationWarehouseId,
                                            @RequestParam(required = false) Optional<Integer> originWarehouseId) {
        if (service.filterByWarehouse(destinationWarehouseId, originWarehouseId).isEmpty()) {
            throw new ResponseStatusException((HttpStatus.NOT_FOUND));
        }
        return service.filterByWarehouse(destinationWarehouseId, originWarehouseId);
    }

    @GetMapping("/filter/customer")
    public List<CollectionParcel> filterByCustomer(@RequestParam(required = false) Optional<Integer> customerId) {
        if (service.filterByCustomer(customerId).isEmpty()){
            throw new ResponseStatusException((HttpStatus.NOT_FOUND));
        }
        return service.filterByCustomer(customerId);
    }

    @GetMapping("/listOnTheWay/")
    public List<Shipment> listShipmentOnTheWay(@RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        if (service.listShipmentOnTheWay(user).isEmpty()){
            throw new ResponseStatusException((HttpStatus.NOT_FOUND));
        }
        return service.listShipmentOnTheWay(user);
    }


    @GetMapping("/listPreparing/")
    public List<Shipment> listShipmentPreparing(@RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        if (service.listShipmentOnTheWay(user).isEmpty()){
            throw new ResponseStatusException((HttpStatus.NOT_FOUND));
        }
        return service.listShipmentPreparing(user);
    }

    @GetMapping("/listCompleted/")
    public List<Shipment> listShipmentCompleted(@RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        if (service.listShipmentOnTheWay(user).isEmpty()){
            throw new ResponseStatusException((HttpStatus.NOT_FOUND));
        }
        return service.listShipmentCompleted(user);
    }

}
