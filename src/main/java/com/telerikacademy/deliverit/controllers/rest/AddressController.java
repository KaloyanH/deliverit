package com.telerikacademy.deliverit.controllers.rest;

import com.telerikacademy.deliverit.controllers.AuthenticationHelper;
import com.telerikacademy.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.deliverit.exceptions.UnauthorizedOperationException;
import com.telerikacademy.deliverit.models.User;
import com.telerikacademy.deliverit.services.ModelMapperAddress;
import com.telerikacademy.deliverit.models.Address;
import com.telerikacademy.deliverit.models.dto.AddressDto;
import com.telerikacademy.deliverit.services.AddressService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/addresses")
public class AddressController {

    private final AddressService service;
    private final AuthenticationHelper authenticationHelper;
    private final ModelMapperAddress modelMapperAddress;

    @Autowired
    public AddressController(AddressService service,
                             AuthenticationHelper authenticationHelper,
                             ModelMapperAddress modelMapperAddress) {
        this.service = service;
        this.authenticationHelper = authenticationHelper;
        this.modelMapperAddress = modelMapperAddress;
    }

    @GetMapping
    @ApiOperation(value = "Returns a list of addresses")
    public List<Address> getAll() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Returns an addresses by id, if an address with such id exists.")
    public Address getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Address create(@RequestHeader HttpHeaders headers, @Valid @RequestBody AddressDto addressDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Address address = modelMapperAddress.fromDto(addressDto);
            service.create(address);
            return address;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Address update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody AddressDto addressDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Address address = modelMapperAddress.fromDto(addressDto, id);
            service.update(address);
            return address;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers,@PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Address address = service.getById(id);
            service.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

}
