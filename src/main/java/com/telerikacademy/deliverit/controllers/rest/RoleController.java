package com.telerikacademy.deliverit.controllers.rest;

import com.telerikacademy.deliverit.controllers.AuthenticationHelper;
import com.telerikacademy.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.deliverit.models.User;
import com.telerikacademy.deliverit.services.ModelMapperRoles;
import com.telerikacademy.deliverit.services.RoleService;
import com.telerikacademy.deliverit.models.Role;
import com.telerikacademy.deliverit.models.dto.RoleDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/api/roles")
public class RoleController {

    private final RoleService service;
    private final AuthenticationHelper authenticationHelper;
    private final ModelMapperRoles modelMapperRoles;

    @Autowired
    public RoleController(RoleService service, AuthenticationHelper authenticationHelper, ModelMapperRoles modelMapperRoles) {
        this.service = service;
        this.authenticationHelper = authenticationHelper;
        this.modelMapperRoles = modelMapperRoles;
    }

    @GetMapping
    public List<Role> getAll() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    public Role getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Role create(@RequestHeader HttpHeaders headers,@Valid @RequestBody RoleDto rollDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Role role = modelMapperRoles.fromDto(rollDto);
            service.create(role, user);
            return role;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Role update(@RequestHeader HttpHeaders headers,@PathVariable int id, @Valid @RequestBody RoleDto roleDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Role role = modelMapperRoles.fromDto(roleDto, id);
            service.update(role, user);
            return role;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public Role delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Role role = service.getById(id);
            service.delete(id, user);
            return role;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
