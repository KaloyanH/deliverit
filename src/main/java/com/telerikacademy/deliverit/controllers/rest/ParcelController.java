package com.telerikacademy.deliverit.controllers.rest;

import com.telerikacademy.deliverit.controllers.AuthenticationHelper;
import com.telerikacademy.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.deliverit.exceptions.UnauthorizedOperationException;
import com.telerikacademy.deliverit.models.User;
import com.telerikacademy.deliverit.services.ModelMapperParcel;
import com.telerikacademy.deliverit.services.ParcelService;
import com.telerikacademy.deliverit.models.Parcel;
import com.telerikacademy.deliverit.models.dto.ParcelDto;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/parcels")
public class ParcelController {

    private final ParcelService parcelService;
    private final ModelMapperParcel modelMapperParcel;
    private final AuthenticationHelper authenticationHelper;


    public ParcelController(ParcelService parcelService, ModelMapperParcel modelMapperParcel, AuthenticationHelper authenticationHelper) {
        this.parcelService = parcelService;
        this.modelMapperParcel = modelMapperParcel;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Parcel> getAll() {
        return parcelService.getAll();
    }

    @GetMapping("/{id}")
    public Parcel getById(@PathVariable int id) {
        try {
            return parcelService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/filter/weight")
    public List<Parcel> filterByWeight(@RequestParam(required = false) Optional<Integer> weight) {
        if (parcelService.filterByWeight(weight).isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return parcelService.filterByWeight(weight);
    }

    @GetMapping("/filter/email")
    public List<Parcel> filterByEmail(@RequestParam(required = false) Optional<String> email) {
        if (parcelService.filterByEmail(email).isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return parcelService.filterByEmail(email);
    }

    @GetMapping("/filter/warehouse")
    public List<Parcel> filterByWarehouse(@RequestParam(required = false) Optional<Integer> warehouseId) {
        if (parcelService.filterByWarehouse(warehouseId).isEmpty()) {
            throw new ResponseStatusException((HttpStatus.NOT_FOUND));
        }
        return parcelService.filterByWarehouse(warehouseId);
    }

    @GetMapping("/filter/category")
    public List<Parcel> filterByCategory(@RequestParam(required = false) Optional<Integer> categoryId) {
        if (parcelService.filterByCategory(categoryId).isEmpty()) {
            throw new ResponseStatusException((HttpStatus.NOT_FOUND));
        }
        return parcelService.filterByCategory(categoryId);
    }

    @GetMapping("/filter")
    public List<Parcel> filter(@RequestParam(required = false) Optional<Integer> weight,
                               @RequestParam(required = false) Optional<String> userEmail,
                               @RequestParam(required = false) Optional<Integer> warehouseId,
                               @RequestParam(required = false) Optional<Integer> categoryId) {

        if (parcelService.filter(weight, userEmail, warehouseId, categoryId).isEmpty()) {
            throw new ResponseStatusException((HttpStatus.NOT_FOUND));
        }
        return parcelService.filter(weight, userEmail, warehouseId, categoryId);
    }

    @PostMapping
    public Parcel create(@RequestHeader HttpHeaders headers, @Valid @RequestBody ParcelDto parcelDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Parcel parcel = modelMapperParcel.fromDto(parcelDto);
            parcel.setUser(user);
            parcelService.create(parcel, user);
            return parcel;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
    }

    @PutMapping("/{id}")
    public Parcel update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody ParcelDto parcelDto) {
        try {
            Parcel parcel = modelMapperParcel.fromDto(parcelDto, id);
            User user = authenticationHelper.tryGetUser(headers);
            parcelService.update(parcel, user);
            return parcel;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            parcelService.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

}
