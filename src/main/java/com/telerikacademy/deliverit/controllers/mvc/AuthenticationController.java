package com.telerikacademy.deliverit.controllers.mvc;

import com.telerikacademy.deliverit.controllers.AuthenticationHelper;
import com.telerikacademy.deliverit.exceptions.AuthenticationFailureException;
import com.telerikacademy.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.deliverit.models.City;
import com.telerikacademy.deliverit.models.Country;
import com.telerikacademy.deliverit.models.User;
import com.telerikacademy.deliverit.models.dto.LoginDto;
import com.telerikacademy.deliverit.models.dto.RegisterDto;
import com.telerikacademy.deliverit.services.CityService;
import com.telerikacademy.deliverit.services.CountryService;
import com.telerikacademy.deliverit.services.ModelMapperUser;
import com.telerikacademy.deliverit.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/auth")
public class AuthenticationController {

    private final UserService userService;
    private final CityService cityService;
    private final CountryService countryService;
    private final AuthenticationHelper authenticationHelper;
    private final ModelMapperUser modelMapperUser;

    @Autowired
    public AuthenticationController(UserService userService,
                                    CityService cityService,
                                    CountryService countryService,
                                    AuthenticationHelper authenticationHelper,
                                    ModelMapperUser modelMapperUser) {
        this.userService = userService;
        this.cityService = cityService;
        this.countryService = countryService;
        this.authenticationHelper = authenticationHelper;
        this.modelMapperUser = modelMapperUser;
    }

    @ModelAttribute("cities")
    public List<City> populateCities() {
        return cityService.getAll();
    }

    @ModelAttribute("countries")
    public List<Country> populateCountries() {
        return countryService.getAll();
    }

    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute("login", new LoginDto());
        return "login";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("login") LoginDto login,
                              BindingResult bindingResult,
                              HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "login";
        }

        try {
            authenticationHelper.verifyAuthentication(login.getUsername(), login.getPassword());
            session.setAttribute("currentUser", login.getUsername());
            return "redirect:/";
        } catch (AuthenticationFailureException e) {
            bindingResult.rejectValue("username", "auth_error", e.getMessage());
            return "login";
        }
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUser");
        return "redirect:/";
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("register", new RegisterDto());
        return "register";
    }

    @PostMapping("/register")
    public String handleRegister(@Valid @ModelAttribute("register") RegisterDto register,
                                 BindingResult bindingResult,
                                 HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "register";
        }

        if (!register.getPassword().equals(register.getPasswordConfirm())) {
            bindingResult.rejectValue("passwordConfirm", "password_error", "Password confirmation should match password.");
            return  "register";
        }

        try {
            User user = modelMapperUser.fromDto(register);
            userService.create(user);
            return "redirect:/auth/login";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("username", "username_error", e.getMessage());
            return "register";
        }
    }
}
