package com.telerikacademy.deliverit.controllers.mvc;


import com.telerikacademy.deliverit.controllers.AuthenticationHelper;
import com.telerikacademy.deliverit.exceptions.AuthenticationFailureException;
import com.telerikacademy.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.deliverit.exceptions.UnauthorizedOperationException;
import com.telerikacademy.deliverit.models.*;
import com.telerikacademy.deliverit.models.dto.ShipmentDto;
import com.telerikacademy.deliverit.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/shipments")
public class ShipmentMvcController {

    private final ShipmentService shipmentService;
    private final AuthenticationHelper authenticationHelper;
    private final ModelMapperShipment modelMapperShipment;
    private final AddressService addressService;
    private final WarehouseService warehouseService;

    @Autowired
    public ShipmentMvcController(ShipmentService shipmentService, AuthenticationHelper authenticationHelper,
                                 ModelMapperShipment modelMapperShipment, AddressService addressService, WarehouseService warehouseService) {
        this.shipmentService = shipmentService;
        this.authenticationHelper = authenticationHelper;
        this.modelMapperShipment = modelMapperShipment;
        this.addressService = addressService;
        this.warehouseService = warehouseService;
    }

    @GetMapping
    public String showAllShipment(Model model, HttpSession session) {
        User user = authenticationHelper.tryGetUser(session);
        if (!user.isEmployee()){
            return "access-denied";
        }
        model.addAttribute("shipments", shipmentService.getAll());
        return "shipments";
    }

    @GetMapping("/listshipmentsOnTheWay")
    public String showAllShipmentsOnTheWay(Model model, HttpSession session) {
        User user = authenticationHelper.tryGetUser(session);
        if (!user.isEmployee()){
            return "access-denied";
        }
        model.addAttribute("shipments", shipmentService.listShipmentOnTheWay(user));
        return "shipment-OnTheWay";
    }

    @GetMapping("/listshipmentsPreparing")
    public String showAllShipmentsPreparing(Model model, HttpSession session) {
        User user = authenticationHelper.tryGetUser(session);
        if (!user.isEmployee()){
            return "access-denied";
        }
        model.addAttribute("shipments", shipmentService.listShipmentPreparing(user));
        return "shipment-Preparing";
    }

    @GetMapping("/listshipmentsCompleted")
    public String showAllShipmentsCompleted(Model model, HttpSession session) {
        User user = authenticationHelper.tryGetUser(session);
        if (!user.isEmployee()){
            return "access-denied";
        }
        model.addAttribute("shipments", shipmentService.listShipmentCompleted(user));
        return "shipment-Completed";
    }

    @GetMapping("/{id}")
    public String showSingleShipment(@PathVariable int id, Model model) {
        try {
            Shipment shipment = shipmentService.getById(id);
            model.addAttribute("shipment", shipment);
            return "shipment";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/update")
    public String showEditShipmentPage(@PathVariable int id, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            Shipment shipment = shipmentService.getById(id);
            ShipmentDto shipmentDto = modelMapperShipment.toDto(shipment);
            List<Address> addressDto = addressService.getAll();
            model.addAttribute("shipmentId", id);
            model.addAttribute("shipment", shipmentDto);
            return "shipment-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/update")
    public String updateShipment(@PathVariable int id,
                                 @Valid @ModelAttribute("shipment") ShipmentDto dto,
                                 BindingResult errors,
                                 Model model,
                                 HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "shipment-update";
        }

        try {
            Shipment shipment = modelMapperShipment.fromDto(dto, id);
            shipmentService.update(shipment, user);

            return "redirect:/shipments";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("firstName", "duplicate_shipment", e.getMessage());
            return "shipment-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @ModelAttribute("originWarehouses")
    public List<Warehouse> populateOriginWarehouses() {
        return warehouseService.getAll();
    }

    @ModelAttribute("destinationWarehouses")
    public List<Warehouse> populateDeliveryWarehouses() {
        return warehouseService.getAll();
    }

    @ModelAttribute("isAuthenticatedCustomer")
    public boolean populateIsAuthenticatedCustomer(HttpSession session) {
        User user = authenticationHelper.tryGetUser(session);
        return session.getAttribute("currentUser") != null && user.isCustomer();
    }

    @ModelAttribute("isAuthenticatedEmployee")
    public boolean populateIsAuthenticatedEmployee(HttpSession session) {
        User user = authenticationHelper.tryGetUser(session);
        return session.getAttribute("currentUser") != null && user.isEmployee();
    }

    @GetMapping("/{id}/delete")
    public String deleteShipment(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            shipmentService.delete(id, user);

            return "redirect:/shipments";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }


    @GetMapping("/new")
    public String showNewShipmentPage(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        model.addAttribute("shipment", new ShipmentDto());
        return "shipment-new";
    }

    @PostMapping("/new")
    public String createShipment(@Valid @ModelAttribute("shipment") ShipmentDto shipmentDto,
                             BindingResult errors,
                             Model model,
                             HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "shipment-new";
        }

        try {
            Shipment shipment = modelMapperShipment.fromDto(shipmentDto);
            shipmentService.create(shipment, user);

            return "redirect:/shipments";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "duplicate_shipment", e.getMessage());
            return "shipment-new";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }
}
