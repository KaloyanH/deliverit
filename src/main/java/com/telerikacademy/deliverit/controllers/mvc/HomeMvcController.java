package com.telerikacademy.deliverit.controllers.mvc;

import com.telerikacademy.deliverit.controllers.AuthenticationHelper;
import com.telerikacademy.deliverit.models.User;
import com.telerikacademy.deliverit.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/")
public class HomeMvcController {

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;


    public HomeMvcController(UserService userService, AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
    }


    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }


    @ModelAttribute("isAuthenticatedEmployee")
    public boolean populateIsAuthenticatedEmployee(HttpSession session) {
        User user = authenticationHelper.tryGetEmployee(session);
        return session.getAttribute("currentUser") != null && user.isEmployee();
    }

    @GetMapping
    public String showHomePage(Model model) {
        model.addAttribute("customersCount", userService.getAllCustomers().size());
        return "index";
    }
}
