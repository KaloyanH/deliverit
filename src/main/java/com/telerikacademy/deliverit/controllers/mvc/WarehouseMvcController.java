package com.telerikacademy.deliverit.controllers.mvc;

import com.telerikacademy.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.deliverit.models.Warehouse;
import com.telerikacademy.deliverit.services.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/warehouses")
public class WarehouseMvcController {

    private final WarehouseService warehouseService;

    @Autowired
    public WarehouseMvcController(WarehouseService warehouseService) {
        this.warehouseService = warehouseService;
    }

    @GetMapping
    public String showAllWarehouses(Model model) {
        model.addAttribute("warehouses", warehouseService.getAll());
        return "warehouses";
    }

    @GetMapping("/{id}")
    public String showSingleWarehouse(@PathVariable int id, Model model) {
        try {
            Warehouse warehouse = warehouseService.getById(id);
            model.addAttribute("warehouse", warehouse);
            return "warehouse";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }
}
