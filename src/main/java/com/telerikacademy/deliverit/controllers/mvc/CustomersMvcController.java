package com.telerikacademy.deliverit.controllers.mvc;

import com.telerikacademy.deliverit.controllers.AuthenticationHelper;
import com.telerikacademy.deliverit.exceptions.AuthenticationFailureException;
import com.telerikacademy.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.deliverit.exceptions.UnauthorizedOperationException;
import com.telerikacademy.deliverit.models.Address;
import com.telerikacademy.deliverit.models.User;
import com.telerikacademy.deliverit.models.dto.UserDto;
import com.telerikacademy.deliverit.services.AddressService;
import com.telerikacademy.deliverit.services.ModelMapperUser;
import com.telerikacademy.deliverit.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/customers")
public class CustomersMvcController {

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final ModelMapperUser modelMapperUser;
    private final AddressService addressService;

    @Autowired
    public CustomersMvcController(UserService userService, AuthenticationHelper authenticationHelper, ModelMapperUser modelMapperUser, AddressService addressService) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.modelMapperUser = modelMapperUser;
        this.addressService = addressService;
    }

    @GetMapping
    public String showAllCustomers(Model model, HttpSession session) {
        User user = authenticationHelper.tryGetUser(session);
        if (!user.isEmployee()){
            return "access-denied";
        }
        model.addAttribute("customers", userService.getAllCustomers());
        return "customers";
    }

    @GetMapping("/{id}")
    public String showSingleCustomer(@PathVariable int id, Model model) {
        try {
            User customer = userService.getById(id);
            model.addAttribute("customer", customer);
            return "customer";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/update")
    public String showEditCustomerPage(@PathVariable int id, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            User user = userService.getById(id);
            UserDto userDto = modelMapperUser.toDto(user);
            List<Address> addressDto = addressService.getAll();
            model.addAttribute("customerId", id);
            model.addAttribute("customer", userDto);
            return "customer-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/update")
    public String updateCustomer(@PathVariable int id,
                             @Valid @ModelAttribute("customer") UserDto dto,
                             BindingResult errors,
                             Model model,
                             HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "customer-update";
        }

        try {
            User user1 = modelMapperUser.fromDto(dto, id);
            userService.update(user1, user);

            return "redirect:/customers";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("firstName", "duplicate_customer", e.getMessage());
            return "customer-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @ModelAttribute("addresses")
    public List<Address> populateAddresses() {
        return addressService.getAll();
    }

    @ModelAttribute("isAuthenticatedCustomer")
    public boolean populateIsAuthenticatedCustomer(HttpSession session) {
        User user = authenticationHelper.tryGetUser(session);
        return session.getAttribute("currentUser") != null && user.isCustomer();
    }

    @ModelAttribute("isAuthenticatedEmployee")
    public boolean populateIsAuthenticatedEmployee(HttpSession session) {
        User user = authenticationHelper.tryGetUser(session);
        return session.getAttribute("currentUser") != null && user.isEmployee();
    }

    @GetMapping("/{id}/delete")
    public String deleteCustomer(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            userService.delete(id, user);

            return "redirect:/customers";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }
}
