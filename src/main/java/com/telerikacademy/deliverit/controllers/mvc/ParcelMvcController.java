package com.telerikacademy.deliverit.controllers.mvc;

import com.telerikacademy.deliverit.controllers.AuthenticationHelper;
import com.telerikacademy.deliverit.exceptions.AuthenticationFailureException;
import com.telerikacademy.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.deliverit.exceptions.UnauthorizedOperationException;
import com.telerikacademy.deliverit.models.*;
import com.telerikacademy.deliverit.models.dto.ParcelDto;
import com.telerikacademy.deliverit.services.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/parcels")
public class ParcelMvcController {

    private final ParcelService parcelService;
    private final UserService userService;
    private final WarehouseService warehouseService;
    private final CategoryServiceImpl categoryService;
    private final ModelMapperParcel modelMapperParcel;
    private final AuthenticationHelper authenticationHelper;


    public ParcelMvcController(ParcelService parcelService,
                               UserService userService,
                               WarehouseService warehouseService,
                               CategoryServiceImpl categoryService,
                               ModelMapperParcel modelMapperParcel,
                               AuthenticationHelper authenticationHelper) {
        this.parcelService = parcelService;
        this.userService = userService;
        this.warehouseService = warehouseService;
        this.categoryService = categoryService;
        this.modelMapperParcel = modelMapperParcel;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isAuthenticatedCustomer")
    public boolean populateIsAuthenticatedCustomer(HttpSession session) {
        User user = authenticationHelper.tryGetUser(session);
        return session.getAttribute("currentUser") != null && user.isCustomer();
    }

    @ModelAttribute("isAuthenticatedEmployee")
    public boolean populateIsAuthenticatedEmployee(HttpSession session) {
        User user = authenticationHelper.tryGetUser(session);
        return session.getAttribute("currentUser") != null && user.isEmployee();
    }

    @ModelAttribute("customers")
    public List<User> populateCustomers() {
        return userService.getAllCustomers();
    }

    @ModelAttribute("warehouses")
    public List<Warehouse> populateWarehouses() {
        return warehouseService.getAll();
    }

    @ModelAttribute("categories")
    public List<Category> populateCategories() {
        return categoryService.getAll();
    }

    @GetMapping
    public String showParcels(Model model, HttpSession session) {
        model.addAttribute("parcels", parcelService.getAll());
        User user = authenticationHelper.tryGetUser(session);
        String userEmail = authenticationHelper.tryGetUser(session).getEmail();
        parcelService.filterByEmail(java.util.Optional.ofNullable(userEmail));
        model.addAttribute("customerCompletedParcels", userService.listUserCompletedParcels(user.getById()));
        model.addAttribute("customerOnTheWayParcels", userService.listUserOnTheWayParcels(user.getById()));

        return "parcels";
    }

    @GetMapping("/{id}")
    public String showSingleParcel(@PathVariable int id, Model model) {
        try {
            Parcel parcel = parcelService.getById(id);
            model.addAttribute("parcel", parcel);
            return "parcel";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/new")
    public String showNewParcelPage(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        model.addAttribute("parcel", new ParcelDto());
        return "parcel-new";
    }

    @PostMapping("/new")
    public String createParcel(@Valid @ModelAttribute("parcel") ParcelDto parcelDto,
                                 BindingResult errors,
                                 Model model,
                                 HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "parcel-new";
        }

        try {
            Parcel parcel = modelMapperParcel.fromDto(parcelDto);
            parcelService.create(parcel, user);

            return "redirect:/parcels";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "duplicate_parcel", e.getMessage());
            return "parcel-new";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteParcel(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            parcelService.delete(id);

            return "redirect:/parcels";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }


    @GetMapping("/{id}/update")
    public String showEditParcelPage(@PathVariable int id, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            Parcel parcel = parcelService.getById(id);
            ParcelDto parcelDto = modelMapperParcel.toDto(parcel);
            model.addAttribute("parcel", parcelDto);
            return "parcel-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/update")
    public String updateParcel(@PathVariable int id,
                                 @Valid @ModelAttribute("parcel") ParcelDto parcelDto,
                                 BindingResult errors,
                                 Model model,
                                 HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "parcel-update";
        }

        try {
            Parcel parcelToUpdate = modelMapperParcel.fromDto(parcelDto, id);
            parcelService.update(parcelToUpdate, user);

            return "redirect:/parcels";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("firstName", "duplicate_customer", e.getMessage());
            return "parcel-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

}
