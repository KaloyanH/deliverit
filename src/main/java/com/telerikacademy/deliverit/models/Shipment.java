package com.telerikacademy.deliverit.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.telerikacademy.deliverit.models.enums.Status;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

import static com.telerikacademy.deliverit.models.enums.Status.*;

@Entity
@Table(name = "shipments")
public class Shipment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "shipment_id")
    private int id;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "origin_warehouse_id")
    private Warehouse originWarehouse;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "destination_warehouse_id")
    private Warehouse destinationWarehouse;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @Column(name = "departure")
    private LocalDate departure;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @Column(name = "arrival")
    private LocalDate arrival;


    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "collections_parcels",
            joinColumns = @JoinColumn(name = "shipment_id"),
            inverseJoinColumns = @JoinColumn(name = "parcel_id")
    )
    private Set<Parcel> parcels;

    public Shipment() {
    }

    public Set<Parcel> getParcels() {
        return parcels;
    }

    public void setParcels(Set<Parcel> parcels) {
        this.parcels = parcels;
    }



    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {

        this.status = calculateStatus(status);
    }

    public Warehouse getOriginWarehouse() {
        return originWarehouse;
    }

    public void setOriginWarehouse(Warehouse originWarehouse) {
        this.originWarehouse = originWarehouse;
    }

    public Warehouse getDestinationWarehouse() {
        return destinationWarehouse;
    }

    public void setDestinationWarehouse(Warehouse destinationWarehouse) {
        this.destinationWarehouse = destinationWarehouse;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getDeparture() {
        return departure;
    }

    public void setDeparture(LocalDate departure) {
        this.departure = departure;
        setArrival(this.arrival);
    }

    public LocalDate getArrival() {
        return arrival;
    }

    public void setArrival(LocalDate arrival) {
        this.arrival = departure.plusDays(5);
    }


    private Status calculateStatus(Status status) {
        LocalDate now = LocalDate.now();


        if (now.isBefore(departure) || now.isEqual(departure)) {
            status = PREPARING;
        } else if (now.isAfter(departure) && now.isBefore(arrival)) {
            status = ON_THE_WAY;

        } else if (now.isAfter(arrival)) {
            status = COMPLETED;
        }

        return status;
    }
}
