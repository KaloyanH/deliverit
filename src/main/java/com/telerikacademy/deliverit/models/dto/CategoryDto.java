package com.telerikacademy.deliverit.models.dto;

import javax.validation.constraints.NotNull;

import javax.validation.constraints.Size;

public class CategoryDto {

    @NotNull(message = "Address can't be empty")
    @Size(min = 5, max = 50, message = "Address should be between 5 and 50 symbols")
    private String name;


    public CategoryDto() {

    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
