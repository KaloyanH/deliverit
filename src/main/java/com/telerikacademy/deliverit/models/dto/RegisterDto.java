package com.telerikacademy.deliverit.models.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

public class RegisterDto {

    @NotEmpty
    @Size(min = 5, max = 50, message = "Password should be between 5 and 50 symbols")
    private String password;

    @NotEmpty(message = "Password confirmation can't be empty")
    @Size(min = 5, max = 50, message = "Password should be between 5 and 50 symbols")
    private String passwordConfirm;

    @NotEmpty(message = "First name can't be empty")
    @Size(min = 2, max = 20, message = "First name should be between 2 and 20 symbols")
    private String firstName;

    @NotEmpty(message = "Last name can't be empty")
    @Size(min = 2, max = 20, message = "Last name should be between 2 and 20 symbols")
    private String lastName;

    @NotEmpty(message = "Email can't be empty")
    @Email
    private String email;

    @NotEmpty(message = "Address can't be empty")
    @Size(max = 50, message = "Address name should be up to 50 symbols")
    private String addressName;

    @PositiveOrZero(message = "cityId should be positive or zero")
    private int cityId;

    @PositiveOrZero(message = "countryId should be positive or zero")
    private int countryId;


    public String getAddressName() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
