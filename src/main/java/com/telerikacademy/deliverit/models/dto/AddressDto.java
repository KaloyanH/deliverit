package com.telerikacademy.deliverit.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;


public class AddressDto {


    @NotNull(message = "Address can't be empty")
    @Size(min = 5, max = 50, message = "Address should be between 5 and 50 symbols")
    private String name;

    @PositiveOrZero(message = "country ID should be positive or zero")
    private int city;

    public AddressDto() {

    }

    public int getCity() {
        return city;
    }

    public void setCity(int city) {
        this.city = city;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
