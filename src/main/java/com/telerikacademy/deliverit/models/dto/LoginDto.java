package com.telerikacademy.deliverit.models.dto;

import javax.validation.constraints.NotEmpty;

public class LoginDto {

    @NotEmpty
    private String email;

    @NotEmpty
    private String password;

    public String getUsername() {
        return email;
    }

    public void setUsername(String username) {
        this.email = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
