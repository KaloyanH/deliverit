package com.telerikacademy.deliverit.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

public class CityDto {

    @NotNull(message = "City name cannot be empty.")
    @Size(min = 3, max = 20, message = "City name must be between 3 and 20 symbols.")
    private String name;

    @PositiveOrZero(message = "countryId should be positive or zero.")
    private int countryId;

    public CityDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }
}
