package com.telerikacademy.deliverit.models.dto;


import com.telerikacademy.deliverit.models.enums.Status;
import org.springframework.format.annotation.DateTimeFormat;


import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.time.LocalDate;


public class ShipmentDto {


    @PositiveOrZero(message = "origin warehouse ID should be positive or zero")
    private int originWarehouse;

    @PositiveOrZero(message = "destination warehouse ID should be positive or zero")
    private int destinationWarehouse;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @Transient
    private LocalDate arrivalDate;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @NotNull(message = "Departure date can't be empty")
    private LocalDate departureDate;

    @Transient
    private Status status;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public ShipmentDto() {

    }

    public LocalDate getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(LocalDate arrivalDate) {
        this.arrivalDate = getDepartureDate().plusDays(5);
    }

    public LocalDate getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(LocalDate departureDate) {
        this.departureDate = departureDate;
    }

    public int getOriginWarehouse() {
        return originWarehouse;
    }

    public void setOriginWarehouse(int originWarehouse) {
        this.originWarehouse = originWarehouse;
    }

    public int getDestinationWarehouse() {
        return destinationWarehouse;
    }

    public void setDestinationWarehouse(int destinationWarehouse) {
        this.destinationWarehouse = destinationWarehouse;
    }


}
