package com.telerikacademy.deliverit.models.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

public class UserDto {


    @NotNull(message = "First name can't be empty")
    @Size(min = 2, max = 20, message = "First name should be between 2 and 20 symbols")
    private String firstName;

    @NotNull(message = "Last name can't be empty")
    @Size(min = 2, max = 20, message = "Last name should be between 2 and 20 symbols")
    private String lastName;

    @PositiveOrZero(message = "Address ID should be positive or zero")
    private int address;

    @NotNull(message = "Email can't be empty")
    @Email
    @Size(min = 5, max = 50, message = "Email should be between 5 and 50 symbols")
    private String email;

    @NotNull(message = "Password can't be empty")
    @Size(min = 5, max = 50, message = "Password should be between 5 and 50 symbols")
    private String password;

    public UserDto() {

    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAddress() {
        return address;
    }

    public void setAddress(int address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
