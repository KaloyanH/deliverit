package com.telerikacademy.deliverit.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

public class ParcelDto {

    @NotNull(message = "Parcel name should be between 3 and 30 symbols.")
    @Size(min = 3, max = 30)
    private String name;

    @PositiveOrZero(message = "User Id should be positive or zero.")
    private int userId;

    @PositiveOrZero(message = "Warehouse Id should be positive or zero.")
    private int warehouseId;

    @PositiveOrZero(message = "Parcel weight should be positive or zero.")
    private int weight;

    @PositiveOrZero(message = "Category Id should be positive or zero.")
    private int categoryId;

    public ParcelDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(int warehouseId) {
        this.warehouseId = warehouseId;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }
}
