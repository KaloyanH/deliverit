package com.telerikacademy.deliverit.models.dto;

import javax.validation.constraints.NotNull;

import javax.validation.constraints.Size;

public class RoleDto {


    @NotNull(message = "Role can't be empty")
    @Size(min = 2, max = 20, message = "Role should be between 2 and 20 symbols")
    private String role;

    public RoleDto() {

    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }


}
