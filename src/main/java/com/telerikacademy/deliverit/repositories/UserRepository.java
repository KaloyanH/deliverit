package com.telerikacademy.deliverit.repositories;


import com.telerikacademy.deliverit.models.Parcel;
import com.telerikacademy.deliverit.models.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {

    List<User> getAll();

    User getById(int id);

    List<User> getAllCustomers();

    User getByFirstName(String firstname);

    User getByLastName(String lastName);

    User getByEmail(String username);

    void create(User user);

    void update(User user);

    void delete(int id);

    List<User> search(Optional<String> search, Optional<String>firstName , Optional<String> lastName , Optional<String> email);

    List<Parcel> listUserParcels(int userId);

    List<Parcel> listUserCompletedParcels(int userId);

    List<Parcel> listUserOnTheWayParcels(int userId);
}
