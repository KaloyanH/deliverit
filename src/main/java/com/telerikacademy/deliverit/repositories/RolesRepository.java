package com.telerikacademy.deliverit.repositories;


import com.telerikacademy.deliverit.models.Role;

import java.util.List;

public interface RolesRepository {

    List<Role> getAll();

    Role getById(int id);

    Role getByRole(String role);

    void create(Role role);

    void update(Role role);

    void delete(int id);
}
