package com.telerikacademy.deliverit.repositories;

import com.telerikacademy.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.deliverit.models.CollectionParcel;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CollectionParcelRepositoryImpl implements CollectionParcelRepository {

    private final SessionFactory sessionFactory;

    public CollectionParcelRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<CollectionParcel> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<CollectionParcel> query = session.createQuery("from CollectionParcel ", CollectionParcel.class);
            return query.list();
        }
    }

    @Override
    public CollectionParcel getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            CollectionParcel collectionParcel = session.get(CollectionParcel.class, id);
            if (collectionParcel == null) {
                throw new EntityNotFoundException("CollectionParcel", id);
            }
            return collectionParcel;
        }
    }
}
