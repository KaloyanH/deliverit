package com.telerikacademy.deliverit.repositories;


import com.telerikacademy.deliverit.models.CollectionParcel;
import com.telerikacademy.deliverit.models.Parcel;
import com.telerikacademy.deliverit.models.Shipment;

import java.util.List;
import java.util.Optional;

public interface ShipmentRepository {

    List<Shipment> getAll();

    Shipment getById(int id);

    void create(Shipment shipment);

    void update(Shipment shipment);

    void delete(int id);

    List<Shipment> filterByWarehouse(Optional<Integer> warehouseId, Optional<Integer> originWarehouseId);

    List<CollectionParcel> filterByCustomer(Optional<Integer> customerId);

    List<Shipment> listShipmentOnTheWay();

    List<Shipment> listShipmentPreparing();

    List<Shipment> listShipmentCompleted();
}
