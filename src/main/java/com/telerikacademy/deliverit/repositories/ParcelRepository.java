package com.telerikacademy.deliverit.repositories;

import com.telerikacademy.deliverit.models.Parcel;

import java.util.List;
import java.util.Optional;

public interface ParcelRepository {

    List<Parcel> getAll();

    Parcel getById(int id);

    Parcel getByName (String name);

    List<Parcel> filterByWeight(Optional<Integer> weight);

    List<Parcel> filterByEmail(Optional<String> userEmail);

    List<Parcel> filterByWarehouse(Optional<Integer> warehouseId);

    List<Parcel> filterByCategory(Optional<Integer> categoryId);

    List<Parcel> filter(Optional<Integer> weight,
                        Optional<String> userEmail,
                        Optional<Integer> warehouseId,
                        Optional<Integer> categoryId);

//    List<Parcel> filter(Optional<Integer> weight,
//                        Optional<Customer> customer,
//                        Optional<Warehouse> warehouse,

//                        Optional<Category> category);

    void create(Parcel parcel);

    void update(Parcel parcel);

    void delete(int id);
}
