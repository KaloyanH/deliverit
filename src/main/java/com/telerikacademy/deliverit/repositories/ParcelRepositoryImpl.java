package com.telerikacademy.deliverit.repositories;

import com.telerikacademy.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.deliverit.models.Parcel;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Repository
public class ParcelRepositoryImpl implements ParcelRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ParcelRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Parcel> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Parcel> query = session.createQuery("from Parcel", Parcel.class);
            return query.list();
        }
    }

    @Override
    public Parcel getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Parcel parcel = session.get(Parcel.class, id);
            if (parcel == null) {
                throw new EntityNotFoundException("Parcel", id);
            }
            return parcel;
        }
    }

    @Override
    public Parcel getByName(String name){
        try (Session session = sessionFactory.openSession()) {
            Query<Parcel> query = session.createQuery("from Parcel where name = :name", Parcel.class);
            query.setParameter("name", name);

            List<Parcel> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Parcel", "name", name);
            }

            return result.get(0);
        }
    }

    @Override
    public List<Parcel> filterByWeight(Optional<Integer> weight) {
        try (Session session = sessionFactory.openSession()) {
            Query<Parcel> query = session.createQuery("from Parcel where weight >= :weight", Parcel.class);
            query.setParameter("weight", weight.orElse(0));
            return query.list();
        }
    }


    @Override
    public List<Parcel> filterByEmail(Optional<String> userEmail) {
        try (Session session = sessionFactory.openSession()) {
            Query<Parcel> query = session.createQuery("from Parcel where user.email like :email", Parcel.class);
            query.setParameter("email", userEmail.orElse(" "));
            return query.list();
        }
    }

    @Override
    public List<Parcel> filterByWarehouse(Optional<Integer> warehouseId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Parcel> query = session.createQuery("from Parcel where warehouse.id = :id", Parcel.class);
            query.setParameter("id", warehouseId.orElse(0));
            return query.list();
        }
    }

    @Override
    public List<Parcel> filterByCategory(Optional<Integer> categoryId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Parcel> query = session.createQuery("from Parcel where category.id = :id", Parcel.class);
            query.setParameter("id", categoryId.orElse(0));
            return query.list();
        }
    }

    @Override
    public List<Parcel> filter(Optional<Integer> weight,
                               Optional<String> userEmail,
                               Optional<Integer> warehouseId,
                               Optional<Integer> categoryId) {
        try (Session session = sessionFactory.openSession()) {
            var queryString = new StringBuilder("from Parcel ");
            var filters = new ArrayList<String>();
            var params = new HashMap<String, Object>();

            weight.ifPresent(value -> {
                filters.add("weight >= :weight");
                params.put("weight", value);
            });

            userEmail.ifPresent(value -> {
                filters.add("user.email = :userEmail");
                params.put("userEmail", value);
            });

            warehouseId.ifPresent(value -> {
                filters.add("warehouse.id = :warehouseId" );
                params.put("warehouseId", value);
            });

            categoryId.ifPresent(value -> {
                filters.add("category.id = :categoryId" );
                params.put("categoryId", value);
            });

            if (!filters.isEmpty()) {
                queryString.append("where ")
                        .append(String.join(" and ", filters));
            }

            Query<Parcel> query = session.createQuery(queryString.toString(), Parcel.class);
            query.setProperties(params);
//            System.out.println(queryString);
            return query.list();
        }
    }

    @Override
    public void create(Parcel parcel) {
        try (Session session = sessionFactory.openSession()) {
            session.save(parcel);
        }
    }

    @Override
    public void update(Parcel parcel) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(parcel);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Parcel parcelToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(parcelToDelete);
            session.getTransaction().commit();
        }
    }
}
