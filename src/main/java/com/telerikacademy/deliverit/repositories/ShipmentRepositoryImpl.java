package com.telerikacademy.deliverit.repositories;

import com.telerikacademy.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.deliverit.models.CollectionParcel;
import com.telerikacademy.deliverit.models.Shipment;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Repository
public class ShipmentRepositoryImpl implements ShipmentRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ShipmentRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Shipment> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Shipment> query = session.createQuery(
                    "from Shipment ", Shipment.class);
            return query.list();
        }
    }

    @Override
    public Shipment getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Shipment shipment = session.get(Shipment.class, id);
            if (shipment == null) {
                throw new EntityNotFoundException("Shipment", id);
            }
            return shipment;
        }
    }


    @Override
    public void create(Shipment shipment) {
        try (Session session = sessionFactory.openSession()) {
            session.save(shipment);
        }
    }

    @Override
    public void update(Shipment shipment) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(shipment);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Shipment shipmentToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(shipmentToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Shipment> filterByWarehouse(Optional<Integer> destinationWarehouseId, Optional<Integer> originWarehouseId) {
        try (Session session = sessionFactory.openSession()) {

            var queryString = new StringBuilder("from Shipment ");
            var filters = new ArrayList<String>();
            var params = new HashMap<String, Object>();


            destinationWarehouseId.ifPresent(value -> {
                filters.add("destinationWarehouse.id = :destinationWarehouseId");
                params.put("destinationWarehouseId", value);
            });

            originWarehouseId.ifPresent(value -> {
                filters.add("originWarehouse.id = :originWarehouseId");
                params.put("originWarehouseId", value);
            });

            if (!filters.isEmpty()) {
                queryString.append("where ")
                        .append(String.join(" and ", filters));
            }

            Query<Shipment> query = session.createQuery(queryString.toString(), Shipment.class);
            query.setProperties(params);
            return query.list();
        }


    }

    @Override
    public List<CollectionParcel> filterByCustomer(Optional<Integer> customerId) {
        try (Session session = sessionFactory.openSession()) {
            Query<CollectionParcel> query = session.createQuery("from CollectionParcel where parcel.user.id = :id",
                    CollectionParcel.class);
            query.setParameter("id", customerId.orElse(0));
            return query.list();
        }
    }

    @Override
    public List<Shipment> listShipmentOnTheWay() {
        try (Session session = sessionFactory.openSession()) {
            Query<Shipment> query = session.createQuery("from Shipment s where s.status = 'ON_THE_WAY'",
                    Shipment.class);
            return query.list();
        }
    }

    @Override
    public List<Shipment> listShipmentPreparing() {
        try (Session session = sessionFactory.openSession()) {
            Query<Shipment> query = session.createQuery("from Shipment s where s.status = 'PREPARING'",
                    Shipment.class);
            return query.list();
        }
    }

    @Override
    public List<Shipment> listShipmentCompleted() {
        try (Session session = sessionFactory.openSession()) {
            Query<Shipment> query = session.createQuery("from Shipment s where s.status = 'COMPLETED'",
                    Shipment.class);
            return query.list();
        }
    }
}

