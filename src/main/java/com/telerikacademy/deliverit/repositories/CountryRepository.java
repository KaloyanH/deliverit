package com.telerikacademy.deliverit.repositories;

import com.telerikacademy.deliverit.models.Country;

import java.util.List;

public interface CountryRepository {

    List<Country> getAll();

    Country getById(int id);
}
