package com.telerikacademy.deliverit.repositories;

import com.telerikacademy.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.deliverit.models.CollectionParcel;
import com.telerikacademy.deliverit.models.Parcel;
import com.telerikacademy.deliverit.models.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(
                    "from User ", User.class);
            return query.list();
        }
    }

    @Override
    public List<User> getAllCustomers(){
        try(Session session = sessionFactory.openSession()){
            Query<User> query = session.createQuery(
                    "from User where role.role = 'customer'", User.class);
            return query.list();
        }
    }

    @Override
    public User getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            if (user == null) {
                throw new EntityNotFoundException("User", id);
            }
            return user;
        }
    }

    @Override
    public User getByFirstName(String firstName) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where firstName = :name", User.class);
            query.setParameter("name", firstName);

            List<User> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "first name", firstName);
            }

            return result.get(0);
        }
    }

    @Override
    public User getByLastName(String lastName) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where lastName = :name", User.class);
            query.setParameter("name", lastName);

            List<User> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "last name", lastName);
            }

            return result.get(0);
        }
    }

    @Override
    public User getByEmail(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where email =:email", User.class);
            query.setParameter("email", username);
            List<User> users = query.list();
            if (users.size() == 0) {
                throw new EntityNotFoundException("User", "username", username);
            }
            return users.get(0);
        }
    }

    @Override
    public void create(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        User userToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(userToDelete);
            session.getTransaction().commit();
        }
    }


    @Override
    public List<User> search(Optional<String> search, Optional<String> firstName, Optional<String> lastName, Optional<String> email) {
        try (Session session = sessionFactory.openSession()) {


            var queryString = new StringBuilder("from User ");
            var filters = new ArrayList<String>();
            var params = new HashMap<String, Object>();

            firstName.ifPresent(value -> {
                filters.add("firstName = :firstname");
                params.put("firstname", value);
            });

            lastName.ifPresent(value -> {
                filters.add("lastName = :lastname");
                params.put("lastname", value);
            });

            email.ifPresent(value -> {
                filters.add("email = :email");
                params.put("email", value);
            });

            if (!filters.isEmpty()) {
                queryString.append("where ")
                        .append(String.join(" or ", filters));
            } else {
                Query<User> query = session.createQuery(
                        "from User where email like :email or firstName like :firstname or lastName like :lastname", User.class);
                query.setParameter("email", "%" + search.get() + "%");
                query.setParameter("firstname", "%" + search.get() + "%");
                query.setParameter("lastname", "%" + search.get() + "%");
                query.setProperties(query);
//                result.stream()
//                .filter(beer -> beer.getName().toLowerCase().contains(name.toLowerCase()))
//                .collect(Collectors.toList());
                return query.list();
            }

            Query<User> query = session.createQuery(queryString.toString(), User.class);
            query.setProperties(params);
            return query.list();
        }


    }

    public List<Parcel> listUserParcels(int userId) {
        try (Session session = sessionFactory.openSession()) {

            List<CollectionParcel> collectionParcels = new ArrayList<>();

            Query<CollectionParcel> query = session.createQuery("from CollectionParcel where parcel.user.id = :id " +
                            "and shipment.status = 'ON_THE_WAY' or parcel.user.id = :id and shipment.status = 'PREPARING'",
                    CollectionParcel.class);

            query.setParameter("id", userId);
            collectionParcels = query.list();
            return collectionParcels.stream().map(CollectionParcel::getParcel).collect(Collectors.toList());

        }
    }

    public List<Parcel> listUserCompletedParcels(int userId) {
        try (Session session = sessionFactory.openSession()) {

            List<CollectionParcel> collectionParcels = new ArrayList<>();

            Query<CollectionParcel> query = session.createQuery("from CollectionParcel where parcel.user.id = :id " +
                            "and shipment.status = 'COMPLETED'",
                    CollectionParcel.class);

            query.setParameter("id", userId);
            collectionParcels = query.list();
            return collectionParcels.stream().map(CollectionParcel::getParcel).collect(Collectors.toList());

        }
    }

    public List<Parcel> listUserOnTheWayParcels(int userId) {
        try (Session session = sessionFactory.openSession()) {

            List<CollectionParcel> collectionParcels = new ArrayList<>();

            Query<CollectionParcel> query = session.createQuery("from CollectionParcel where parcel.user.id = :id " +
                            "and shipment.status = 'ON_THE_WAY'",
                    CollectionParcel.class);

            query.setParameter("id", userId);
            collectionParcels = query.list();
            return collectionParcels.stream().map(CollectionParcel::getParcel).collect(Collectors.toList());

        }
    }
}

