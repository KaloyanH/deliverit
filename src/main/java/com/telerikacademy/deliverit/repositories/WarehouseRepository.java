package com.telerikacademy.deliverit.repositories;

import com.telerikacademy.deliverit.models.Warehouse;

import java.util.List;

public interface WarehouseRepository {

    List<Warehouse> getAll();

    Warehouse getById(int id);
}
