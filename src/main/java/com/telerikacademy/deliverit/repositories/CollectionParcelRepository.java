package com.telerikacademy.deliverit.repositories;

import com.telerikacademy.deliverit.models.CollectionParcel;

import java.util.List;

public interface CollectionParcelRepository {

    List<CollectionParcel> getAll();

    CollectionParcel getById(int id);
}
