package com.telerikacademy.deliverit.repositories;

import com.telerikacademy.deliverit.models.Address;

import java.util.List;

public interface AddressRepository {

    List<Address> getAll();

    Address getById(int id);

    Address getByName(String name);

    void create(Address address);

    void update(Address address);

    void delete(int id);

}
