package com.telerikacademy.deliverit.repositories;

import com.telerikacademy.deliverit.models.City;

import java.util.List;

public interface CityRepository {

    List<City> getAll();

    City getById(int id);
}
