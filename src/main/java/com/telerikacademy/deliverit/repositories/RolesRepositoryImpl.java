package com.telerikacademy.deliverit.repositories;

import com.telerikacademy.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.deliverit.models.Role;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public class RolesRepositoryImpl implements RolesRepository {

    private final SessionFactory sessionFactory;

    public RolesRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<Role> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Role> query = session.createQuery(
                    "from Role ", Role.class);
            return query.list();
        }
    }

    @Override
    public Role getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Role role = session.get(Role.class, id);
            if (role == null) {
                throw new EntityNotFoundException("Role", id);
            }
            return role;
        }
    }

    @Override
    public Role getByRole(String role) {
        try (Session session = sessionFactory.openSession()) {
            Query<Role> query = session.createQuery("from Role where role = :name", Role.class);
            query.setParameter("name", role);

            List<Role> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Role", "name", role);
            }

            return result.get(0);
        }
    }

    @Override
    public void create(Role role) {
        try (Session session = sessionFactory.openSession()) {
            session.save(role);
        }
    }

    @Override
    public void update(Role role) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(role);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Role roleToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(roleToDelete);
            session.getTransaction().commit();
        }
    }
}
