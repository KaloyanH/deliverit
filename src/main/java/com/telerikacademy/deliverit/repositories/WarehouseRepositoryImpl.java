package com.telerikacademy.deliverit.repositories;

import com.telerikacademy.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.deliverit.models.Warehouse;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class WarehouseRepositoryImpl implements WarehouseRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public WarehouseRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Warehouse> getAll() {
        try(Session session = sessionFactory.openSession()){
            Query<Warehouse> query = session.createQuery("from Warehouse", Warehouse.class);
            return query.list();
        }
    }

    @Override
    public Warehouse getById(int id) {
        try(Session session = sessionFactory.openSession()){
            Warehouse warehouse = session.get(Warehouse.class, id);
            if(warehouse == null){
                throw new EntityNotFoundException("Warehouse", id);
            }
            return warehouse;
        }
    }
}
