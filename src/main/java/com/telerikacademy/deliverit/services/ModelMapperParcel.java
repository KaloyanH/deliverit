package com.telerikacademy.deliverit.services;

import com.telerikacademy.deliverit.models.Category;
import com.telerikacademy.deliverit.models.Parcel;
import com.telerikacademy.deliverit.models.User;
import com.telerikacademy.deliverit.models.Warehouse;
import com.telerikacademy.deliverit.models.dto.ParcelDto;
import com.telerikacademy.deliverit.repositories.CategoryRepository;
import com.telerikacademy.deliverit.repositories.ParcelRepository;
import com.telerikacademy.deliverit.repositories.UserRepository;
import com.telerikacademy.deliverit.repositories.WarehouseRepository;
import org.springframework.stereotype.Component;

@Component
public class ModelMapperParcel {

    private final ParcelRepository parcelRepository;
    private final UserRepository userRepository;
    private final WarehouseRepository warehouseRepository;
    private final CategoryRepository categoryRepository;

    public ModelMapperParcel(ParcelRepository parcelRepository, UserRepository userRepository, WarehouseRepository warehouseRepository, CategoryRepository categoryRepository) {
        this.parcelRepository = parcelRepository;
        this.userRepository = userRepository;
        this.warehouseRepository = warehouseRepository;
        this.categoryRepository = categoryRepository;
    }

    public Parcel fromDto(ParcelDto parcelDto){
        Parcel parcel = new Parcel();
        dtoToObject(parcelDto, parcel);
        return parcel;
    }

    public Parcel fromDto(ParcelDto parcelDto, int id){
        Parcel parcel = parcelRepository.getById(id);
        dtoToObject(parcelDto, parcel);
        return parcel;
    }

    public ParcelDto toDto(Parcel parcel){
        ParcelDto parcelDto = new ParcelDto();
        parcelDto.setName(parcel.getName());
        parcelDto.setUserId(parcel.getUser().getById());
        parcelDto.setWarehouseId(parcel.getWarehouse().getId());
        parcelDto.setWeight(parcel.getWeight());
        parcelDto.setCategoryId(parcel.getCategory().getId());

        return parcelDto;
    }

    private void dtoToObject(ParcelDto parcelDto, Parcel parcel) {
        parcel.setName(parcelDto.getName());

        User user = userRepository.getById(parcelDto.getUserId());
        parcel.setUser(user);

        Warehouse warehouse = warehouseRepository.getById(parcelDto.getWarehouseId());
        parcel.setWarehouse(warehouse);

        parcel.setWeight(parcelDto.getWeight());

        Category category = categoryRepository.getById(parcelDto.getCategoryId());
        parcel.setCategory(category);
    }
}
