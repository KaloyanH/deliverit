package com.telerikacademy.deliverit.services;

import com.telerikacademy.deliverit.models.Address;
import com.telerikacademy.deliverit.models.User;

import java.util.List;

public interface AddressService {

    List<Address> getAll();

    Address getById(int id);

    void create(Address address);

    void update(Address address);

    void delete(int id, User user);
}
