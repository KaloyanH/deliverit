package com.telerikacademy.deliverit.services;


import com.telerikacademy.deliverit.models.Category;
import com.telerikacademy.deliverit.models.User;

import java.util.List;

public interface CategoryService {

    List<Category> getAll();

    Category getById(int id);

    void create(Category category);

    void update(Category category);

    void delete(int id, User user);
}
