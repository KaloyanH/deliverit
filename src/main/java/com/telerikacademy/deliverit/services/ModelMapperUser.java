package com.telerikacademy.deliverit.services;


import com.telerikacademy.deliverit.models.*;
import com.telerikacademy.deliverit.models.dto.RegisterDto;
import com.telerikacademy.deliverit.models.dto.UserDto;
import com.telerikacademy.deliverit.repositories.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;

@Component
public class ModelMapperUser {

    private final AddressRepository addressRepository;
    private final UserRepository userRepository;
    private final RolesRepository roleRepository;
    private final CityRepository cityRepository;
    private final CountryRepository countryRepository;

    @Autowired
    public ModelMapperUser(AddressRepository addressRepository,
                           UserRepository userRepository,
                           RolesRepository roleRepository,
                           CityRepository cityRepository,
                           CountryRepository countryRepository) {
        this.addressRepository = addressRepository;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.cityRepository = cityRepository;
        this.countryRepository = countryRepository;
    }

    public User fromDto(UserDto userDto) {
        User user = new User();
        dtoToObject(userDto, user);
        user.setRoles(new HashSet<>());
        return user;
    }

    public UserDto toDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setEmail(user.getEmail());
        userDto.setPassword(user.getPassword());
        userDto.setAddress(user.getAddress().getId());
        return userDto;
    }

    public User fromDto(RegisterDto registerDto) {
        User user = new User();
        Address address = new Address();
        Country country = countryRepository.getById(registerDto.getCountryId());
        City city = cityRepository.getById(registerDto.getCityId());

        user.setPassword(registerDto.getPassword());
        user.setFirstName(registerDto.getFirstName());
        user.setLastName(registerDto.getLastName());
        user.setEmail(registerDto.getEmail());

        address.setName(registerDto.getAddressName());
        city.setCountry(country);
        address.setCity(city);

        user.setAddress(address);

        Role role = roleRepository.getById(1);
        user.setRole(role);

        return user;
    }

    public User fromDto(UserDto userDto, int id) {
        User user = userRepository.getById(id);
        dtoToObject(userDto, user);
        return user;
    }

    private void dtoToObject(UserDto userDto, User user) {
        Address address = addressRepository.getById(userDto.getAddress());
        Role role = roleRepository.getById(1);
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setAddress(address);
        user.setEmail(userDto.getEmail());
        user.setPassword(userDto.getPassword());
        user.setRole(role);
    }
}
