package com.telerikacademy.deliverit.services;

import com.telerikacademy.deliverit.models.Warehouse;
import com.telerikacademy.deliverit.repositories.WarehouseRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WarehouseServiceImpl implements WarehouseService {

    private final WarehouseRepository warehouseRepository;

    public WarehouseServiceImpl(WarehouseRepository warehouseRepository) {
        this.warehouseRepository = warehouseRepository;
    }

    public List<Warehouse> getAll(){
       return warehouseRepository.getAll();
    }

    public Warehouse getById(int id){
        return warehouseRepository.getById(id);
    }
}
