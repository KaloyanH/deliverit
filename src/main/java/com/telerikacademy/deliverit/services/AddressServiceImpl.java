package com.telerikacademy.deliverit.services;

import com.telerikacademy.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.deliverit.exceptions.UnauthorizedOperationException;
import com.telerikacademy.deliverit.models.Address;
import com.telerikacademy.deliverit.models.User;
import com.telerikacademy.deliverit.repositories.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressServiceImpl implements AddressService {

    private final AddressRepository repository;
    public static final String MODIFY_ADDRESS_ERROR_MESSAGE = "Only company employees can remove an address.";

    @Autowired
    public AddressServiceImpl(AddressRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Address> getAll() {
        return repository.getAll();
    }

    @Override
    public Address getById(int id) {
        return repository.getById(id);
    }

    @Override
    public void create(Address address) {
        boolean duplicateExists = true;
        try {
            repository.getByName(address.getName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Address", "name", address.getName());
        }

        repository.create(address);
    }

    @Override
    public void update(Address address) {
        boolean duplicateExists = true;
        try {
            Address existingAddress = repository.getByName(address.getName());
            if (existingAddress.getId() == address.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Address", "name", address.getName());
        }

        repository.update(address);
    }

    @Override
    public void delete(int id, User user) {
        if (!user.isEmployee()) {
            throw new UnauthorizedOperationException(MODIFY_ADDRESS_ERROR_MESSAGE);
        }
        repository.delete(id);
    }
}
