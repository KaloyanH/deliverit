package com.telerikacademy.deliverit.services;

import com.telerikacademy.deliverit.models.Country;

import java.util.List;

public interface CountryService {

    List<Country> getAll();

    Country getById(int id);
}
