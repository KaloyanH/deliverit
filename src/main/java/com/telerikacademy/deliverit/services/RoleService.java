package com.telerikacademy.deliverit.services;


import com.telerikacademy.deliverit.models.Role;
import com.telerikacademy.deliverit.models.User;

import java.util.List;

public interface RoleService {

    List<Role> getAll();

    Role getById(int id);

    void create(Role role, User user);

    void update(Role role, User user);

    void delete(int id, User user);
}
