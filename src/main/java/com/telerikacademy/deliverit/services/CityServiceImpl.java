package com.telerikacademy.deliverit.services;

import com.telerikacademy.deliverit.models.City;
import com.telerikacademy.deliverit.repositories.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityServiceImpl implements CityService {

    private final CityRepository cityRepository;

    @Autowired
    public CityServiceImpl(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    public List<City> getAll(){
        return cityRepository.getAll();
    }

    public City getById(int id){
        return cityRepository.getById(id);
    }
}
