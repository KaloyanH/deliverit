package com.telerikacademy.deliverit.services;

import com.telerikacademy.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.deliverit.exceptions.UnauthorizedOperationException;
import com.telerikacademy.deliverit.models.Parcel;
import com.telerikacademy.deliverit.models.User;
import com.telerikacademy.deliverit.repositories.ParcelRepository;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;

@Service
public class ParcelServiceImpl implements ParcelService {

    private final ParcelRepository parcelRepository;
    public static final String MODIFY_PARCEL_ERROR_MESSAGE = "Only owner or employee can modify a parcel.";

    public ParcelServiceImpl(ParcelRepository parcelRepository) {
        this.parcelRepository = parcelRepository;
    }


    @Override
    public List<Parcel> getAll() {
        return parcelRepository.getAll();
    }

    @Override
    public Parcel getByName(String name){
        return parcelRepository.getByName(name);
    }

    @Override
    public Parcel getById(int id) {
        return parcelRepository.getById(id);
    }

    @Override
    public List<Parcel> filterByWeight(Optional<Integer> weight) {
        return parcelRepository.filterByWeight(weight);
    }

    @Override
    public List<Parcel> filterByEmail(Optional<String> email) {
        return parcelRepository.filterByEmail(email);
    }

    @Override
    public List<Parcel> filterByWarehouse(Optional<Integer> warehouseId){
        return parcelRepository.filterByWarehouse(warehouseId);
    }

    @Override
    public List<Parcel> filterByCategory(Optional<Integer> categoryId) {
        return parcelRepository.filterByCategory(categoryId);
    }

    @Override
    public List<Parcel> filter(Optional<Integer> weight, Optional<String> userEmail, Optional<Integer> warehouseId, Optional<Integer> categoryId) {
        return parcelRepository.filter(weight, userEmail, warehouseId, categoryId);
    }

    @Override
    public void create(Parcel parcel, User user) {
        boolean duplicateExists = true;

        if(!user.isEmployee()){
            throw new UnauthorizedOperationException(MODIFY_PARCEL_ERROR_MESSAGE);
        }

        try {
            parcelRepository.getById(parcel.getId());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Parcel", "name", parcel.getName());
        }

        parcelRepository.create(parcel);

    }

    @Override
    public void update(Parcel parcel, User user) {

        Parcel parcelToUpdate = parcelRepository.getById(parcel.getId());
        if (!parcelToUpdate.getUser().equals(user) && !user.isEmployee()) {
            throw new UnauthorizedOperationException(MODIFY_PARCEL_ERROR_MESSAGE);
        }

        boolean duplicateExists = true;
        try {
            Parcel existingParcel = parcelRepository.getById(parcel.getId());
            if (existingParcel.getId() == parcel.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Parcel", "name", parcel.getName());
        }

        parcelRepository.update(parcel);

    }

    @Override
    public void delete(int id) {
        parcelRepository.delete(id);

    }
}
