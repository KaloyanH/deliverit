package com.telerikacademy.deliverit.services;

import com.telerikacademy.deliverit.models.City;

import java.util.List;

public interface CityService {

    List<City> getAll();

    City getById(int id);
}
