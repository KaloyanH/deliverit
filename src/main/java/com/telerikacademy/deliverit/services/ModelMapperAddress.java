package com.telerikacademy.deliverit.services;

import com.telerikacademy.deliverit.models.Address;
import com.telerikacademy.deliverit.models.City;
import com.telerikacademy.deliverit.models.dto.AddressDto;
import com.telerikacademy.deliverit.repositories.AddressRepository;
import com.telerikacademy.deliverit.repositories.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ModelMapperAddress {

    private final AddressRepository addressRepository;
    private final CityRepository cityRepository;

    @Autowired
    public ModelMapperAddress(AddressRepository addressRepository, CityRepository cityRepository) {
        this.addressRepository = addressRepository;
        this.cityRepository = cityRepository;
    }

    public Address fromDto(AddressDto addressDto) {
        Address address = new Address();
        dtoToObject(addressDto, address);
        return address;
    }

    public Address fromDto(AddressDto addressDto, int id) {
        Address address = addressRepository.getById(id);
        dtoToObject(addressDto, address);
        return address;
    }

    private void dtoToObject(AddressDto addressDto, Address address) {
        City city = cityRepository.getById(addressDto.getCity());
        address.setName(addressDto.getName());
        address.setCity(city);
    }
}
