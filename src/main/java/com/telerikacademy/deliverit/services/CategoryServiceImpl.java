package com.telerikacademy.deliverit.services;

import com.telerikacademy.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.deliverit.exceptions.UnauthorizedOperationException;
import com.telerikacademy.deliverit.models.Category;
import com.telerikacademy.deliverit.models.User;
import com.telerikacademy.deliverit.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository repository;
    public static final String MODIFY_CATEGORY_ERROR_MESSAGE = "Only company employees can remove an category.";

    @Autowired
    public CategoryServiceImpl(CategoryRepository repository) {
        this.repository = repository;
    }


    @Override
    public List<Category> getAll() {
        return repository.getAll();
    }

    @Override
    public Category getById(int id) {
        return repository.getById(id);
    }

    @Override
    public void create(Category category) {
        boolean duplicateExists = true;
        try {
            repository.getByName(category.getName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Category", "name", category.getName());
        }

        repository.create(category);
    }

    @Override
    public void update(Category category) {
        boolean duplicateExists = true;
        try {
            Category existingCategory = repository.getByName(category.getName());
            if (existingCategory.getId() == category.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Category", "name", category.getName());
        }

        repository.update(category);
    }

    @Override
    public void delete(int id, User user) {
        if(!user.isEmployee()){
            throw new UnauthorizedOperationException(MODIFY_CATEGORY_ERROR_MESSAGE);
        }
        repository.delete(id);
    }
}
