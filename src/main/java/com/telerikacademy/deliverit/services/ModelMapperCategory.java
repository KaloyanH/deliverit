package com.telerikacademy.deliverit.services;

import com.telerikacademy.deliverit.models.Category;
import com.telerikacademy.deliverit.models.dto.CategoryDto;
import com.telerikacademy.deliverit.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ModelMapperCategory {

    private final CategoryRepository categoryRepository;


    @Autowired
    public ModelMapperCategory(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;

    }

    public Category fromDto(CategoryDto categoryDto) {
        Category category = new Category();
        dtoToObject(categoryDto, category);
        return category;
    }

    public Category fromDto(CategoryDto categoryDto, int id) {
        Category category = categoryRepository.getById(id);
        dtoToObject(categoryDto, category);
        return category;
    }

    private void dtoToObject(CategoryDto categoryDto, Category category) {

        category.setName(categoryDto.getName());

    }
}
