package com.telerikacademy.deliverit.services;


import com.telerikacademy.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.deliverit.exceptions.EntityNotFoundException;

import com.telerikacademy.deliverit.exceptions.UnauthorizedOperationException;
import com.telerikacademy.deliverit.models.Parcel;
import com.telerikacademy.deliverit.models.User;
import com.telerikacademy.deliverit.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository repository;
    public static final String MODIFY_USER_ERROR_MESSAGE = "Only company employees can remove or update an user.";

    @Autowired
    public UserServiceImpl(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<User> getAll(Optional<String> search) {
        return repository.getAll();
    }

    @Override
    public List<User> getAllCustomers() {
        return repository.getAllCustomers();
    }

    @Override
    public User getById(int id) {
        return repository.getById(id);
    }

    @Override
    public User getByFirstName(String username) {
        return repository.getByFirstName(username);
    }

    @Override
    public User getByEmail(String username) {
        return repository.getByEmail(username);
    }


    @Override
    public void create(User user) {
        boolean duplicateExists = true;
        try {
            repository.getByEmail(user.getEmail());

        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }

        repository.create(user);
    }

    @Override
    public void update(User user, User employee) {

        if (!employee.isEmployee()) {
            throw new UnauthorizedOperationException(MODIFY_USER_ERROR_MESSAGE);
        }

        boolean duplicateExists = true;
        try {
            User existingUser = repository.getByFirstName(user.getFirstName());
            if (existingUser.getById() == user.getById()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        repository.update(user);
    }

    @Override
    public void delete(int id, User user) {
        if (!user.isEmployee()) {
            throw new UnauthorizedOperationException(MODIFY_USER_ERROR_MESSAGE);
        }
        repository.delete(id);
    }

    @Override
    public List<User> search(Optional<String> search, Optional<String> firstName, Optional<String> lastName, Optional<String> email, User employee) {
        if (!employee.isEmployee()) {
            throw new UnauthorizedOperationException(MODIFY_USER_ERROR_MESSAGE);
        }
        if (search.isEmpty() && firstName.isEmpty() && lastName.isEmpty() && email.isEmpty()){

            return repository.getAll();
        }
        return repository.search(search, firstName, lastName, email);
    }

    @Override
    public List<Parcel> listUserParcels(int userId) {
        return repository.listUserParcels(userId);
    }

    @Override
    public List<Parcel> listUserCompletedParcels(int userId) {
        return repository.listUserCompletedParcels(userId);
    }

    @Override
    public List<Parcel> listUserOnTheWayParcels(int userId) {
        return repository.listUserOnTheWayParcels(userId);
    }
}
