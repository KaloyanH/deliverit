package com.telerikacademy.deliverit.services;

import com.telerikacademy.deliverit.models.Warehouse;

import java.util.List;

public interface WarehouseService {

    List<Warehouse> getAll();

    Warehouse getById(int id);
}
