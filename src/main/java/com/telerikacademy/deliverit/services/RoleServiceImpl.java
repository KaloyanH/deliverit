package com.telerikacademy.deliverit.services;

import com.telerikacademy.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.deliverit.exceptions.UnauthorizedOperationException;
import com.telerikacademy.deliverit.models.Role;
import com.telerikacademy.deliverit.models.User;
import com.telerikacademy.deliverit.repositories.RolesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    private final RolesRepository repository;
    public static final String MODIFY_ROLE_ERROR_MESSAGE = "Only company employees can remove an role.";

    @Autowired
    public RoleServiceImpl(RolesRepository repository) {
        this.repository = repository;
    }


    @Override
    public List<Role> getAll() {
        return repository.getAll();
    }

    @Override
    public Role getById(int id) {
        return repository.getById(id);
    }

    @Override
    public void create(Role role, User user) {
        if (!user.isEmployee()) {
            throw new UnauthorizedOperationException(MODIFY_ROLE_ERROR_MESSAGE);
        }
        boolean duplicateExists = true;
        try {
            repository.getByRole(role.getRole());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Role", "name", role.getRole());
        }

        repository.create(role);
    }

    @Override
    public void update(Role role, User user) {
        if (!user.isEmployee()) {
            throw new UnauthorizedOperationException(MODIFY_ROLE_ERROR_MESSAGE);
        }
        boolean duplicateExists = true;
        try {
            Role existingRole = repository.getByRole(role.getRole());
            if (existingRole.getId() == role.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Role", "name", role.getRole());
        }

        repository.update(role);
    }

    @Override
    public void delete(int id, User user) {
        if (!user.isEmployee()) {
            throw new UnauthorizedOperationException(MODIFY_ROLE_ERROR_MESSAGE);
        }
        repository.delete(id);
    }
}
