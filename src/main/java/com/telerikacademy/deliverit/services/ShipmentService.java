package com.telerikacademy.deliverit.services;


import com.telerikacademy.deliverit.models.CollectionParcel;
import com.telerikacademy.deliverit.models.Shipment;
import com.telerikacademy.deliverit.models.User;


import java.util.List;
import java.util.Optional;

public interface ShipmentService {
    List<Shipment> getAll();

    Shipment getById(int id);

    void create(Shipment shipment, User user);

    void update(Shipment shipment, User employee);

    void delete(int id, User user);

    List<Shipment> filterByWarehouse(Optional<Integer> destinationWarehouseId, Optional<Integer> originWarehouseId);

    List<CollectionParcel> filterByCustomer(Optional<Integer> customerId);

    List<Shipment> listShipmentOnTheWay(User user);

    List<Shipment> listShipmentPreparing(User user);

    List<Shipment> listShipmentCompleted(User user);


}
