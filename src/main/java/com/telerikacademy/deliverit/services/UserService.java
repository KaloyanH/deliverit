package com.telerikacademy.deliverit.services;

import com.telerikacademy.deliverit.models.Parcel;
import com.telerikacademy.deliverit.models.User;

import java.util.List;
import java.util.Optional;


public interface UserService {

    List<User> getAll(Optional<String> search);

    List<User> getAllCustomers();

    User getById(int id);

    User getByFirstName(String firstName);

    User getByEmail(String username);

    void create(User user);

    void update(User user, User employee);

    void delete(int id, User user);

    List<User> search(Optional<String> search, Optional<String> firstName,
                      Optional<String> lastName, Optional<String> email, User employee);

    List<Parcel> listUserParcels(int userId);

    List<Parcel> listUserCompletedParcels(int userId);

    List<Parcel> listUserOnTheWayParcels(int userId);
}
