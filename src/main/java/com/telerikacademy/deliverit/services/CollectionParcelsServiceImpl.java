package com.telerikacademy.deliverit.services;


import com.telerikacademy.deliverit.models.CollectionParcel;
import com.telerikacademy.deliverit.repositories.CollectionParcelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;




import java.util.List;

@Service
public class CollectionParcelsServiceImpl implements CollectionParcelsService {

    private final CollectionParcelRepository collectionParcelRepository;
    public static final String MODIFY_COLLECTIONPARCEL_ERROR_MESSAGE = "Only owner or employee " +
            "can get their parcel information.";

    @Autowired
    public CollectionParcelsServiceImpl(CollectionParcelRepository collectionParcelRepository) {
        this.collectionParcelRepository = collectionParcelRepository;
    }

    @Override
    public List<CollectionParcel> getAll() {

        return collectionParcelRepository.getAll();
    }

    @Override
    public CollectionParcel getById(int id) {
        return collectionParcelRepository.getById(id);
    }
}
