package com.telerikacademy.deliverit.services;

import com.telerikacademy.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.deliverit.exceptions.UnauthorizedOperationException;
import com.telerikacademy.deliverit.models.CollectionParcel;
import com.telerikacademy.deliverit.models.Shipment;
import com.telerikacademy.deliverit.models.User;
import com.telerikacademy.deliverit.repositories.ShipmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ShipmentServiceImpl implements ShipmentService {


    private ShipmentRepository repository;
    public static final String MODIFY_SHIPMENT_ERROR_MESSAGE = "Only company employees can modify shipments.";

    @Autowired
    public ShipmentServiceImpl(ShipmentRepository shipmentRepository) {
        this.repository = shipmentRepository;
    }


    @Override
    public List<Shipment> getAll() {
        return repository.getAll();
    }

    @Override
    public Shipment getById(int id) {
        return repository.getById(id);
    }

    @Override
    public void create(Shipment shipment, User user) {
        if (!user.isEmployee()) {
            throw new UnauthorizedOperationException(MODIFY_SHIPMENT_ERROR_MESSAGE);
        }
        boolean duplicateExists = true;
        try {
            repository.getById(shipment.getId());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Shipment", shipment.getId());
        }

        repository.create(shipment);
    }

    @Override
    public void update(Shipment shipment, User employee) {
        if (!employee.isEmployee()) {
            throw new UnauthorizedOperationException(MODIFY_SHIPMENT_ERROR_MESSAGE);
        }
        repository.update(shipment);
    }

    @Override
    public void delete(int id, User user) {
        if (!user.isEmployee()) {
            throw new UnauthorizedOperationException(MODIFY_SHIPMENT_ERROR_MESSAGE);
        }
        repository.delete(id);
    }

    @Override
    public List<Shipment> filterByWarehouse(Optional<Integer> destinationWarehouseId, Optional<Integer> originWarehouseId) {
        return repository.filterByWarehouse(destinationWarehouseId, originWarehouseId);
    }

    @Override
    public List<CollectionParcel> filterByCustomer(Optional<Integer> customerId) {
        return repository.filterByCustomer(customerId);
    }

    @Override
    public List<Shipment> listShipmentOnTheWay(User user) {
        if (!user.isEmployee()) {
            throw new UnauthorizedOperationException(MODIFY_SHIPMENT_ERROR_MESSAGE);
        }
        return repository.listShipmentOnTheWay();
    }

    @Override
    public List<Shipment> listShipmentPreparing(User user) {
        if (!user.isEmployee()) {
            throw new UnauthorizedOperationException(MODIFY_SHIPMENT_ERROR_MESSAGE);
        }
        return repository.listShipmentPreparing();
    }

    @Override
    public List<Shipment> listShipmentCompleted(User user) {
        if (!user.isEmployee()) {
            throw new UnauthorizedOperationException(MODIFY_SHIPMENT_ERROR_MESSAGE);
        }
        return repository.listShipmentCompleted();
    }

}
