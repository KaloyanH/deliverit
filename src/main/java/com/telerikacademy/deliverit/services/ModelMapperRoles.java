package com.telerikacademy.deliverit.services;

import com.telerikacademy.deliverit.models.Role;
import com.telerikacademy.deliverit.models.dto.RoleDto;
import com.telerikacademy.deliverit.repositories.RolesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ModelMapperRoles {

    private final RolesRepository rolesRepository;


    @Autowired
    public ModelMapperRoles(RolesRepository rolesRepository) {
        this.rolesRepository = rolesRepository;

    }

    public Role fromDto(RoleDto roleDto) {
        Role role = new Role();
        dtoToObject(roleDto, role);
        return role;
    }

    public Role fromDto(RoleDto roleDto, int id) {
        Role role = rolesRepository.getById(id);
        dtoToObject(roleDto, role);
        return role;
    }


    private void dtoToObject(RoleDto roleDto, Role role) {

        role.setRole(roleDto.getRole());

    }
}
