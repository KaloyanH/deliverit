package com.telerikacademy.deliverit.services;

import com.telerikacademy.deliverit.models.Shipment;
import com.telerikacademy.deliverit.models.Warehouse;
import com.telerikacademy.deliverit.models.dto.ShipmentDto;
import com.telerikacademy.deliverit.repositories.ShipmentRepository;
import com.telerikacademy.deliverit.repositories.WarehouseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ModelMapperShipment {

    private final ShipmentRepository shipmentRepository;
    private final WarehouseRepository warehouseRepository;

    @Autowired
    public ModelMapperShipment(ShipmentRepository shipmentRepository, WarehouseRepository warehouseRepository) {
        this.shipmentRepository = shipmentRepository;
        this.warehouseRepository = warehouseRepository;
    }

    public Shipment fromDto(ShipmentDto shipmentDto) {
        Shipment shipment = new Shipment();
        dtoToObject(shipmentDto, shipment);
        return shipment;
    }

    public Shipment fromDto(ShipmentDto shipmentDto, int id) {
        Shipment shipment = shipmentRepository.getById(id);
        dtoToObject(shipmentDto, shipment);
        return shipment;
    }

    private void dtoToObject(ShipmentDto shipmentDto, Shipment shipment) {
        Warehouse originWarehouse = warehouseRepository.getById(shipmentDto.getOriginWarehouse());
        Warehouse destinationWarehouse = warehouseRepository.getById(shipmentDto.getDestinationWarehouse());
        shipment.setDeparture(shipmentDto.getDepartureDate());
        shipment.setStatus(shipmentDto.getStatus());
        shipment.setOriginWarehouse(originWarehouse);
        shipment.setDestinationWarehouse(destinationWarehouse);

    }

    public ShipmentDto toDto(Shipment shipment) {
        ShipmentDto shipmentDto = new ShipmentDto();
        shipmentDto.setOriginWarehouse(shipment.getOriginWarehouse().getId());
        shipmentDto.setDestinationWarehouse(shipment.getDestinationWarehouse().getId());
        shipmentDto.setDepartureDate(shipment.getDeparture());
        shipmentDto.setArrivalDate(shipment.getArrival());
        shipmentDto.setStatus(shipment.getStatus());
        return shipmentDto;
    }
}
