package com.telerikacademy.deliverit.services;


import com.telerikacademy.deliverit.models.CollectionParcel;


import java.util.List;


public interface CollectionParcelsService {


    List<CollectionParcel> getAll();

    CollectionParcel getById(int id);

}
