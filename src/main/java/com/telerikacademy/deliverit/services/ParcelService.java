package com.telerikacademy.deliverit.services;

import com.telerikacademy.deliverit.models.Parcel;
import com.telerikacademy.deliverit.models.User;

import java.util.List;
import java.util.Optional;

public interface ParcelService {

    List<Parcel> getAll();

    Parcel getById(int id);

    Parcel getByName(String name);

    List<Parcel> filterByWeight(Optional<Integer> weight);

    List<Parcel> filterByEmail(Optional<String> email);

    List<Parcel> filterByWarehouse(Optional<Integer> warehouseId);

    List<Parcel> filterByCategory(Optional<Integer> categoryId);

    List<Parcel> filter(Optional<Integer> weight,
                        Optional<String> userEmail,
                        Optional<Integer> warehouseId,
                        Optional<Integer> categoryId);

    void create(Parcel parcel, User user);

    void update(Parcel parcel, User user);

    void delete(int id);
}
