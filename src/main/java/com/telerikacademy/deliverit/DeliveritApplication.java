package com.telerikacademy.deliverit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class DeliveritApplication {

    public static void main(String[] args) {
        SpringApplication.run(DeliveritApplication.class, args);
    }

}
