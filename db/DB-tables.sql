create or replace table categories
(
    category_id int auto_increment
        primary key,
    category_name varchar(20) not null
);

create or replace table countries
(
    country_id int auto_increment
        primary key,
    country_name varchar(50) not null
);

create or replace table cities
(
    city_id int auto_increment
        primary key,
    city_name varchar(50) not null,
    country_id int not null,
    constraint cities_countries_country_id_fk
        foreign key (country_id) references countries (country_id)
);

create or replace table addresses
(
    address_id int auto_increment
        primary key,
    city_id int not null,
    address_name varchar(50) null,
    constraint addresses_city_id_fk
        foreign key (city_id) references cities (city_id)
);

create or replace table roles
(
    role_id int auto_increment
        primary key,
    name varchar(20) not null
);

create or replace table users
(
    user_id int auto_increment
        primary key,
    first_name varchar(20) not null,
    last_name varchar(50) not null,
    address_id int null,
    email varchar(50) not null,
    password varchar(50) not null,
    constraint users_email_uindex
        unique (email)
);

create or replace table user_roles
(
    role_id int not null,
    user_id int not null,
    constraint user_roles_roles_id_fk
        foreign key (role_id) references roles (role_id),
    constraint user_roles_user_id_fk
        foreign key (user_id) references users (user_id)
);

create or replace index customers_address_id_fk
    on users (address_id);

create or replace table warehouses
(
    warehouse_id int auto_increment
        primary key,
    address_id int not null,
    warehouse_name varchar(30) not null,
    constraint warehouses_address_id_fk
        foreign key (address_id) references addresses (address_id)
);

create or replace table parcels
(
    parcel_id int auto_increment
        primary key,
    parcel_name varchar(50) not null,
    warehouse_id int not null,
    weight int not null,
    category_id int not null,
    pick_up_from_warehouse tinyint(1) default 1 not null,
    user_id int not null,
    constraint parcels_category_id_fk
        foreign key (category_id) references categories (category_id),
    constraint parcels_users_user_id_fk
        foreign key (user_id) references users (user_id),
    constraint parcels_warehouse_id_fk
        foreign key (warehouse_id) references warehouses (warehouse_id)
);

create or replace table shipments
(
    shipment_id int auto_increment
        primary key,
    origin_warehouse_id int not null,
    destination_warehouse_id int not null,
    departure datetime not null,
    arrival datetime not null,
    status varchar(20) not null,
    constraint shipments_destination_id_fk
        foreign key (destination_warehouse_id) references warehouses (warehouse_id),
    constraint shipments_original_id_fk
        foreign key (origin_warehouse_id) references warehouses (warehouse_id)
);

create or replace table collections_parcels
(
    collection_id int auto_increment
        primary key,
    parcel_id int not null,
    shipment_id int not null,
    constraint collections_parcel_id_fk
        foreign key (parcel_id) references parcels (parcel_id),
    constraint collections_shipment_id_fk
        foreign key (shipment_id) references shipments (shipment_id)
);
