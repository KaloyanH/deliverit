-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.6.4-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for deliverit
DROP DATABASE IF EXISTS `deliverit`;
CREATE DATABASE IF NOT EXISTS `deliverit` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `deliverit`;

-- Dumping structure for table deliverit.addresses
DROP TABLE IF EXISTS `addresses`;
CREATE TABLE IF NOT EXISTS `addresses` (
                                           `address_id` int(11) NOT NULL AUTO_INCREMENT,
                                           `city_id` int(11) NOT NULL,
                                           `address_name` varchar(50) DEFAULT NULL,
                                           PRIMARY KEY (`address_id`),
                                           KEY `addresses_city_id_fk` (`city_id`),
                                           CONSTRAINT `addresses_city_id_fk` FOREIGN KEY (`city_id`) REFERENCES `cities` (`city_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Dumping data for table deliverit.addresses: ~15 rows (approximately)
/*!40000 ALTER TABLE `addresses` DISABLE KEYS */;
INSERT IGNORE INTO `addresses` (`address_id`, `city_id`, `address_name`) VALUES
                                                                             (1, 1, 'ul. Han Kubrat'),
                                                                             (2, 4, 'Bentredesi Cd.'),
                                                                             (3, 3, 'Par. Leoforou Kifisou'),
                                                                             (4, 2, 'Strada Vasile Conta'),
                                                                             (5, 1, 'ul. Chiprovci'),
                                                                             (6, 1, '12, Prodan Tarakchiev Str.'),
                                                                             (7, 3, 'Solonos 51'),
                                                                             (8, 2, ' Bd. Theodor Pallady, nr. 287, sector 3'),
                                                                             (9, 4, 'Zubeyde Hanim, 06070'),
                                                                             (10, 1, 'ul. Car Samuil 2'),
                                                                             (14, 2, '34 Bulgaria blvd.'),
                                                                             (15, 1, '34 Bulgaria blvd.'),
                                                                             (16, 1, 'c'),
                                                                             (18, 1, '34 Bulgaria blvd.'),
                                                                             (19, 1, '34 Bulgaria blvd.');
/*!40000 ALTER TABLE `addresses` ENABLE KEYS */;

-- Dumping structure for table deliverit.categories
DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
                                            `category_id` int(11) NOT NULL AUTO_INCREMENT,
                                            `category_name` varchar(20) NOT NULL,
                                            PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table deliverit.categories: ~5 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT IGNORE INTO `categories` (`category_id`, `category_name`) VALUES
                                                                     (1, 'Electronics'),
                                                                     (2, 'Medical'),
                                                                     (3, 'Clothing'),
                                                                     (4, 'Food'),
                                                                     (5, 'Mechanical');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table deliverit.cities
DROP TABLE IF EXISTS `cities`;
CREATE TABLE IF NOT EXISTS `cities` (
                                        `city_id` int(11) NOT NULL AUTO_INCREMENT,
                                        `city_name` varchar(50) NOT NULL,
                                        `country_id` int(11) NOT NULL,
                                        PRIMARY KEY (`city_id`),
                                        KEY `cities_countries_country_id_fk` (`country_id`),
                                        CONSTRAINT `cities_countries_country_id_fk` FOREIGN KEY (`country_id`) REFERENCES `countries` (`country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table deliverit.cities: ~7 rows (approximately)
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
INSERT IGNORE INTO `cities` (`city_id`, `city_name`, `country_id`) VALUES
                                                                       (1, 'Sofia', 1),
                                                                       (2, 'Bucharest', 4),
                                                                       (3, 'Atina', 2),
                                                                       (4, 'Ankara', 3),
                                                                       (5, 'Kavala', 2),
                                                                       (6, 'Instanbul', 3),
                                                                       (7, 'Solun', 2);
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;

-- Dumping structure for table deliverit.collections_parcels
DROP TABLE IF EXISTS `collections_parcels`;
CREATE TABLE IF NOT EXISTS `collections_parcels` (
                                                     `collection_id` int(11) NOT NULL AUTO_INCREMENT,
                                                     `parcel_id` int(11) NOT NULL,
                                                     `shipment_id` int(11) NOT NULL,
                                                     PRIMARY KEY (`collection_id`),
                                                     KEY `collections_parcel_id_fk` (`parcel_id`),
                                                     KEY `collections_shipment_id_fk` (`shipment_id`),
                                                     CONSTRAINT `collections_parcel_id_fk` FOREIGN KEY (`parcel_id`) REFERENCES `parcels` (`parcel_id`),
                                                     CONSTRAINT `collections_shipment_id_fk` FOREIGN KEY (`shipment_id`) REFERENCES `shipments` (`shipment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table deliverit.collections_parcels: ~7 rows (approximately)
/*!40000 ALTER TABLE `collections_parcels` DISABLE KEYS */;
INSERT IGNORE INTO `collections_parcels` (`collection_id`, `parcel_id`, `shipment_id`) VALUES
                                                                                           (1, 1, 1),
                                                                                           (4, 2, 7),
                                                                                           (5, 3, 7),
                                                                                           (6, 4, 11),
                                                                                           (7, 5, 11),
                                                                                           (8, 6, 1),
                                                                                           (9, 12, 2);
/*!40000 ALTER TABLE `collections_parcels` ENABLE KEYS */;

-- Dumping structure for table deliverit.countries
DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
                                           `country_id` int(11) NOT NULL AUTO_INCREMENT,
                                           `country_name` varchar(50) NOT NULL,
                                           PRIMARY KEY (`country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table deliverit.countries: ~10 rows (approximately)
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT IGNORE INTO `countries` (`country_id`, `country_name`) VALUES
                                                                  (1, 'Bulgaria'),
                                                                  (2, 'Greece'),
                                                                  (3, 'Turkey'),
                                                                  (4, 'Romania'),
                                                                  (5, 'Serbia'),
                                                                  (6, 'Cherna Gora'),
                                                                  (7, 'France'),
                                                                  (8, 'England'),
                                                                  (9, 'Amerika'),
                                                                  (11, 'Armenia');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;

-- Dumping structure for table deliverit.parcels
DROP TABLE IF EXISTS `parcels`;
CREATE TABLE IF NOT EXISTS `parcels` (
                                         `parcel_id` int(11) NOT NULL AUTO_INCREMENT,
                                         `parcel_name` varchar(50) NOT NULL,
                                         `warehouse_id` int(11) NOT NULL,
                                         `weight` int(11) NOT NULL,
                                         `category_id` int(11) NOT NULL,
                                         `pick_up_from_warehouse` tinyint(1) NOT NULL DEFAULT 1,
                                         `user_id` int(11) NOT NULL,
                                         PRIMARY KEY (`parcel_id`),
                                         KEY `parcels_category_id_fk` (`category_id`),
                                         KEY `parcels_users_user_id_fk` (`user_id`),
                                         KEY `parcels_warehouse_id_fk` (`warehouse_id`),
                                         CONSTRAINT `parcels_category_id_fk` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`),
                                         CONSTRAINT `parcels_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
                                         CONSTRAINT `parcels_warehouse_id_fk` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`warehouse_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Dumping data for table deliverit.parcels: ~10 rows (approximately)
/*!40000 ALTER TABLE `parcels` DISABLE KEYS */;
INSERT IGNORE INTO `parcels` (`parcel_id`, `parcel_name`, `warehouse_id`, `weight`, `category_id`, `pick_up_from_warehouse`, `user_id`) VALUES
                                                                                                                                            (1, 'telephone', 1, 1, 1, 2, 15),
                                                                                                                                            (2, 'microwave', 2, 30, 1, 1, 15),
                                                                                                                                            (3, 'paracetamol', 3, 1, 2, 1, 15),
                                                                                                                                            (4, 'car brake', 4, 10, 5, 2, 15),
                                                                                                                                            (5, 'trousers', 1, 2, 3, 1, 15),
                                                                                                                                            (6, 'lobster', 2, 2, 4, 2, 15),
                                                                                                                                            (12, 'SUV', 1, 999, 2, 1, 15),
                                                                                                                                            (13, 'bike', 1, 666, 2, 1, 15),
                                                                                                                                            (14, 'computer', 2, 20, 1, 1, 15),
                                                                                                                                            (19, 'Vitoshko lale', 2, 2, 4, 1, 15);
/*!40000 ALTER TABLE `parcels` ENABLE KEYS */;

-- Dumping structure for table deliverit.roles
DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
                                       `role_id` int(11) NOT NULL AUTO_INCREMENT,
                                       `name` varchar(20) NOT NULL,
                                       PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table deliverit.roles: ~2 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT IGNORE INTO `roles` (`role_id`, `name`) VALUES
                                                   (1, 'customer'),
                                                   (2, 'employee');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table deliverit.shipments
DROP TABLE IF EXISTS `shipments`;
CREATE TABLE IF NOT EXISTS `shipments` (
                                           `shipment_id` int(11) NOT NULL AUTO_INCREMENT,
                                           `origin_warehouse_id` int(11) NOT NULL,
                                           `destination_warehouse_id` int(11) NOT NULL,
                                           `departure` datetime NOT NULL,
                                           `arrival` datetime NOT NULL,
                                           `status` varchar(20) NOT NULL,
                                           PRIMARY KEY (`shipment_id`),
                                           KEY `shipments_destination_id_fk` (`destination_warehouse_id`),
                                           KEY `shipments_original_id_fk` (`origin_warehouse_id`),
                                           CONSTRAINT `shipments_destination_id_fk` FOREIGN KEY (`destination_warehouse_id`) REFERENCES `warehouses` (`warehouse_id`),
                                           CONSTRAINT `shipments_original_id_fk` FOREIGN KEY (`origin_warehouse_id`) REFERENCES `warehouses` (`warehouse_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Dumping data for table deliverit.shipments: ~11 rows (approximately)
/*!40000 ALTER TABLE `shipments` DISABLE KEYS */;
INSERT IGNORE INTO `shipments` (`shipment_id`, `origin_warehouse_id`, `destination_warehouse_id`, `departure`, `arrival`, `status`) VALUES
                                                                                                                                        (1, 2, 2, '2021-11-10 00:00:00', '2021-11-15 00:00:00', 'COMPLETED'),
                                                                                                                                        (2, 2, 1, '2021-10-30 00:00:00', '2021-11-04 00:00:00', 'COMPLETED'),
                                                                                                                                        (7, 4, 4, '2021-10-30 00:00:00', '2021-11-04 00:00:00', 'ON_THE_WAY'),
                                                                                                                                        (8, 1, 2, '2021-11-25 00:00:00', '2021-11-30 00:00:00', 'PREPARING'),
                                                                                                                                        (9, 1, 2, '2021-10-01 00:00:00', '2021-10-06 00:00:00', 'ON_THE_WAY'),
                                                                                                                                        (10, 1, 2, '2021-11-03 00:00:00', '2021-11-08 00:00:00', 'PREPARING'),
                                                                                                                                        (11, 1, 1, '2021-10-29 00:00:00', '2021-11-03 00:00:00', 'COMPLETED'),
                                                                                                                                        (12, 3, 4, '2021-11-30 00:00:00', '2021-12-05 00:00:00', 'PREPARING'),
                                                                                                                                        (13, 4, 4, '2021-12-30 00:00:00', '2022-01-04 00:00:00', 'PREPARING'),
                                                                                                                                        (18, 1, 3, '2022-10-10 00:00:00', '2022-10-15 00:00:00', 'PREPARING'),
                                                                                                                                        (19, 4, 2, '2021-11-17 00:00:00', '2021-11-22 00:00:00', 'PREPARING');
/*!40000 ALTER TABLE `shipments` ENABLE KEYS */;

-- Dumping structure for table deliverit.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
                                       `user_id` int(11) NOT NULL AUTO_INCREMENT,
                                       `first_name` varchar(20) NOT NULL,
                                       `last_name` varchar(50) NOT NULL,
                                       `address_id` int(11) NOT NULL,
                                       `email` varchar(50) NOT NULL,
                                       `password` varchar(50) NOT NULL,
                                       `role_id` int(11) NOT NULL,
                                       PRIMARY KEY (`user_id`),
                                       UNIQUE KEY `users_email_uindex` (`email`),
                                       KEY `customers_address_id_fk` (`address_id`),
                                       KEY `users_fk` (`role_id`),
                                       CONSTRAINT `users_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- Dumping data for table deliverit.users: ~10 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT IGNORE INTO `users` (`user_id`, `first_name`, `last_name`, `address_id`, `email`, `password`, `role_id`) VALUES
                                                                                                                    (2, 'Kaloyan', 'Haralampiev', 1, 'kaloyan@gmail.com', 'pass1', 2),
                                                                                                                    (3, 'Yordan', 'Georgiev', 1, 'yordan@gmail.com', 'pass1', 2),
                                                                                                                    (5, 'Customer1', 'Customer1', 1, 'cusromer1@mail.com', 'pass1', 1),
                                                                                                                    (6, 'Customer2', 'Customer2', 1, 'customer2@mail.com', 'pass1', 1),
                                                                                                                    (7, 'Customer3', 'Customer3', 1, 'customer3@mail.com', 'pass1', 1),
                                                                                                                    (8, 'Customer4', 'Customer4', 1, 'customer4@mail.com', 'pass1', 1),
                                                                                                                    (9, 'Customer5', 'Customer5', 1, 'customer5@mail.com', 'pass1', 1),
                                                                                                                    (10, 'Customer6', 'Customer6', 1, 'customer6@mail.com', 'pass1', 1),
                                                                                                                    (11, 'Customer7', 'Customer7', 1, 'customer7@gmail.com', 'pass1', 1),
                                                                                                                    (15, 'Dancho', 'Customer', 1, 'dancho@mail.com', 'pass1', 1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table deliverit.user_roles
DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE IF NOT EXISTS `user_roles` (
                                            `role_id` int(11) NOT NULL,
                                            `user_id` int(11) NOT NULL,
                                            KEY `user_roles_roles_id_fk` (`role_id`),
                                            KEY `user_roles_user_id_fk` (`user_id`),
                                            CONSTRAINT `user_roles_roles_id_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`),
                                            CONSTRAINT `user_roles_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table deliverit.user_roles: ~8 rows (approximately)
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT IGNORE INTO `user_roles` (`role_id`, `user_id`) VALUES
                                                           (2, 2),
                                                           (2, 3),
                                                           (1, 5),
                                                           (1, 6),
                                                           (1, 7),
                                                           (1, 8),
                                                           (1, 9),
                                                           (1, 10);
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;

-- Dumping structure for table deliverit.warehouses
DROP TABLE IF EXISTS `warehouses`;
CREATE TABLE IF NOT EXISTS `warehouses` (
                                            `warehouse_id` int(11) NOT NULL AUTO_INCREMENT,
                                            `address_id` int(11) NOT NULL,
                                            `warehouse_name` varchar(30) NOT NULL,
                                            PRIMARY KEY (`warehouse_id`),
                                            KEY `warehouses_address_id_fk` (`address_id`),
                                            CONSTRAINT `warehouses_address_id_fk` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`address_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table deliverit.warehouses: ~4 rows (approximately)
/*!40000 ALTER TABLE `warehouses` DISABLE KEYS */;
INSERT IGNORE INTO `warehouses` (`warehouse_id`, `address_id`, `warehouse_name`) VALUES
                                                                                     (1, 6, 'UniMasters Logistics'),
                                                                                     (2, 7, 'Luggage Storage Central Athens'),
                                                                                     (3, 8, 'CHEMCO TRADE Warehouse'),
                                                                                     (4, 9, 'Novaser Yapi Depo');
/*!40000 ALTER TABLE `warehouses` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
